```
open import 1Lab.Data.Vector
open import 1Lab.Data.List
open import 1Lab.Coeq
open import 1Lab.Base

open import Algebra.Monoid
open import Algebra.Theory

open import Category.Functor.Adjoints.Monadic
open import Category.Constructions.Monad hiding (Free⊣Forget)
open import Category

open Homomorphism
open is-Equiv
open Functor

module Algebra.Monoid.Category.Monadic {ℓ : Level} where
```

# Mon is monadic over Set

```
private
  Comparison¯¹ : Functor (Eilenberg-Moore (L∘R Free⊣Forget)) (Mon {ℓ})
  F₀ Comparison¯¹ alg = record {
    model  = alg.object ;
    models = record {
      interpretation = λ where
      unit nil              → alg.ν nil
      ⊗    (x :: x₁ :: nil) → alg.ν (cons x (cons x₁ nil))
    ; sound = λ where
      associativity (x :: x₁ :: x₂ :: nil) →
        alg.ν (cons x (cons (alg.ν (cons x₁ (cons x₂ nil))) nil))                 ≡⟨ ap (λ e → alg.ν (cons e _)) (sym (happly alg.ν-unit _)) ⟩
        alg.ν (map alg.ν (cons (cons x nil) (cons (cons x₁ (cons x₂ nil)) nil)))  ≡⟨ happly alg.ν-mult _ ⟩
        alg.ν (fold M (cons (cons x (cons x₁ nil)) (cons (cons x₂ nil) nil)))     ≡⟨ sym (happly alg.ν-mult _) ⟩
        alg.ν (map alg.ν (cons (cons x (cons x₁ nil)) (cons (cons x₂ nil) nil)))  ≡⟨ ap (λ e → alg.ν (cons _ (cons e _))) (happly alg.ν-unit _) ⟩
        alg.ν (cons (alg.ν (cons x (cons x₁ nil))) (cons x₂ nil))                 ∎
      left-identity (x :: nil) →
        sym (
          alg.ν (cons (alg.ν nil) (cons x nil))                    ≡⟨ ap (λ e → alg.ν (cons _ (cons e nil))) (sym (happly alg.ν-unit x)) ⟩
          alg.ν (cons (alg.ν nil) (cons (alg.ν (cons x nil)) nil)) ≡⟨ refl ⟩
          alg.ν (map alg.ν (cons nil (cons (cons x nil) nil)))     ≡⟨ happly alg.ν-mult _ ⟩
          alg.ν (cons x nil)                                       ≡⟨ happly alg.ν-unit _ ⟩
          x                                                        ∎
        )
      right-identity (x :: args) →
        sym (
          alg.ν (cons x (cons (alg.ν nil) nil))                    ≡⟨ ap (λ e → alg.ν (cons e _)) (sym (happly alg.ν-unit x)) ⟩
          alg.ν (cons (alg.ν (cons x nil)) (cons (alg.ν nil) nil)) ≡⟨ refl ⟩
          alg.ν (map alg.ν (cons (cons x nil) (cons nil nil)))     ≡⟨ happly alg.ν-mult _ ⟩
          alg.ν (cons x nil)                                       ≡⟨ happly alg.ν-unit _ ⟩
          x                                                        ∎
        ) } }
    where
      module alg = _-Algebra alg
      M = F₀ Algebra.Monoid.Free alg.object 
  F₁ Comparison¯¹ record { morphism = morphism ; commutes = commutes } =
    record
      { function    = morphism
      ; homomorphic = λ where
          unit nil → happly commutes _
          ⊗ (x :: y :: nil) → happly commutes _
      }
  F-id Comparison¯¹ = Homomorphism≡ λ x → refl
  F-∘ Comparison¯¹ f g = Homomorphism≡ λ x → refl
```

```
Mon-monadic : is-Monadic Free⊣Forget
G Mon-monadic = Comparison¯¹
```

After constructing the weak inverse to the `comparison functor`{.Agda
ident=Comparison}, we must construct the two natural isomorphisms
exhibiting G as an inverse.

```
linv Mon-monadic = niso where
  niso : _ ≅ _
  lemma : (x : _) (x₁ : List (_-Algebra.object x))
        → fold (F₀ Comparison¯¹ x) x₁
        ≡ _-Algebra.ν x (map (λ x → x) x₁)
  lemma alg nil = refl
  lemma alg (cons x y) rewrite →rewrite (lemma alg y) =
    alg.ν (cons x (cons (alg.ν (map (λ x → x) y)) nil))                    ≡⟨ ap (λ e → alg.ν (cons e _)) (sym (happly alg.ν-unit _)) ⟩
    alg.ν (cons (alg.ν (cons x nil)) (cons (alg.ν (map (λ x → x) y)) nil)) ≡⟨ happly alg.ν-mult _  ⟩
    alg.ν (cons x (map (λ x → x) y ++ nil))                                ≡⟨ ap (λ e → alg.ν (cons x e)) (sym (++-id-left _)) ⟩
    alg.ν (cons x (map (λ x → x) y))                                       ∎
    where
      module alg = _-Algebra alg
  
  lemma' : (x : _) (x₁ : List (_-Algebra.object x))
         → fold (F₀ Comparison¯¹ x) (map (λ x → x) x₁)
         ≡ _-Algebra.ν x x₁
  lemma' x x₁ =
    fold (F₀ Comparison¯¹ x) (map (λ x → x) x₁) ≡⟨ ap (fold _) (map-id x₁) ⟩
    fold (F₀ Comparison¯¹ x) x₁                 ≡⟨ lemma x x₁ ⟩
    _-Algebra.ν x (map (λ x → x) x₁)            ≡⟨ ap (_-Algebra.ν x) (map-id x₁) ⟩
    _-Algebra.ν x x₁                            ∎

  _≅_.to niso = 
     NT (λ x → record { morphism = λ x → x ; commutes = fun-ext (lemma x) })
        λ x y f → Algebra=>≡ refl
  _≅_.from niso =
    NT (λ x → record { morphism = λ x₁ → x₁ ; commutes = sym (fun-ext (lemma' x)) })
       λ x y f → Algebra=>≡ refl
  _≅_.to-from niso = Algebra=>≡ refl
  _≅_.from-to niso = Algebra=>≡ refl
```

```
rinv Mon-monadic = niso where
  mop : (m : Model monoid) → _ → _ → _
  mop m x y = _⊨_.interpretation (Model.models m) ⊗ (x :: y :: nil)

  munit : (m : Model monoid) → _
  munit m = _⊨_.interpretation (Model.models m) unit nil

  niso : _ ≅ _
  _≅_.to niso =
    NT (λ x → record { function = λ x → x
                     ; homomorphic = λ where
                         unit nil → refl
                         ⊗ (a :: b :: nil) →
                          mop x a (mop x b (munit x)) ≡⟨ ap (mop x a) (sym (_⊨_.sound (Model.models x) right-identity (b :: nil))) ⟩
                          mop x a b                   ∎
                     })
    λ x y f → Homomorphism≡ λ _ → refl
  _≅_.from niso =
    NT (λ x → record { function = λ x → x
                     ; homomorphic = λ where
                        unit nil → refl
                        ⊗ (a :: b :: nil) →
                          mop x a b                   ≡⟨ ap (mop x a) (_⊨_.sound (Model.models x) right-identity (b :: nil)) ⟩
                          mop x a (mop x b (munit x)) ∎
                     })
       λ x y f → Homomorphism≡ λ _ → refl
  _≅_.to-from niso = Homomorphism≡ λ x → refl
  _≅_.from-to niso = Homomorphism≡ λ x → refl
```

<!--
```
_ = _-Algebra
```
-->
