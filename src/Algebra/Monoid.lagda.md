```agda
open import 1Lab.Data.Finite
open import 1Lab.Data.Vector
open import 1Lab.Data.List
open import 1Lab.Coeq
open import 1Lab.Base

open import Algebra.Theory

open import Category.Functor.Adjoints
open import Category

import Category.Diagrams

open _=>_ hiding (op)

module Algebra.Monoid where
```

```
monoid : Theory lzero lzero
```

# The Theory of Monoids

The category of monoids can be described concretely as [the category of
models] for an [algebraic theory], the `theory of monoids`{.Agda
ident=monoid}. Unfolding this definition into something readable we get
that a _monoid_ is:

- A type $X$,
- an inhabitant $\mathrm{id} : X$ (the `unit`{.Agda}),
- a binary operation $(\otimes) : X \to X \to X$,

Satisfying the following laws:

- `associativity`{.Agda}: $x \otimes (y \otimes z) = (x \otimes y) \otimes z$
- `left-identity`{.Agda}: $x = \mathrm{id} \otimes x$
- `right-identity`{.Agda}: $x = x \otimes \mathrm{id}$

[the category of models]: agda://Algebra.Theory#_-Models
[algebraic theory]: agda://Algebra.Theory#Theory

```agda
data Monoid-operations : Set where
  unit ⊗ : Monoid-operations

data Monoid-laws : Set where
  associativity left-identity right-identity : Monoid-laws
```

The unit element is a nullary operation of the signature, and the
operation $\otimes$ has arity 2.

```agda
Signature.operations (Theory.signature monoid) = Monoid-operations
Signature.o-arities  (Theory.signature monoid) unit = 0
Signature.o-arities  (Theory.signature monoid) ⊗    = 2

Theory.laws monoid = Monoid-laws
```

<details>
<summary>
Construction of the terms representing the left- and right-hand-sides of
the equations for the theory of monoids. In Agda, these turn out to be
ugly and very green.
</summary>

```agda
Theory.l-arities monoid associativity = 3
Theory.l-relates monoid associativity =
    op ⊗ ( var fzero
        :: op ⊗ (var (fsuc fzero) :: var (fsuc (fsuc fzero)) :: nil)
        :: nil
         )
  , op ⊗ ( op ⊗ (var fzero :: var (fsuc fzero) :: nil)
        :: var (fsuc (fsuc fzero))
        :: nil
        )

Theory.l-arities monoid left-identity = 1
Theory.l-relates monoid left-identity =
    var fzero
  , op ⊗ (op unit nil :: var fzero :: nil)

Theory.l-arities monoid right-identity = 1
Theory.l-relates monoid right-identity = 
    var fzero
  , op ⊗ (var fzero :: op unit nil :: nil)
```
</details>

Since this definition characterises not what a monoid is, but what the
_category_ of monoids is, we automatically get the definition of a
_monoid homomorphism_. A monoid homomorphism $f : M \to M\prime$ is a
function between the underlying sets of $M$ and $M\prime$ satisfying:

- $f(\mathrm{id}_M) = \mathrm{id}_{M\prime}$
- $f(a \otimes_M b) = f(a) \otimes_{M\prime} f(b)$

```agda
Monoid : {ℓ : _} → Set (lsuc ℓ)
Monoid = Model monoid
```

A `Monoid`{.Agda} is a model for the theory `monoid`{.Agda}.

```
Mon : {ℓ : _} → Category (lsuc ℓ) ℓ
Mon = monoid -Models
```

The `category of monoids`{.Agda ident=Mon} is the category of
`Monoid`{.Agda}s and monoid homomorphisms.

## Concrete examples

The classic example of a monoid is $(\mathbb{N}, 0, +)$ - the monoid of
_natural numbers under addition_. This is not "the natural numbers
monoid" - there are at least two of those (addition, multiplication).
Rather it's one example of how the `natural numbers`{.Agda ident=Nat} _[model]_
the theory of monoids.

[model]: agda://Algebra.Theory#_⊨_

```agda
N-monoid : Nat ⊨ monoid
N-monoid = mon where
  assoc : (x y z : _) → x + (y + z) ≡ (x + y) + z
```

It has to be shown that $(+)$ is an _associative_ operation, which is
done by induction on $x$.

```
  assoc zero y z = refl
  assoc (suc x) y z = ap suc (assoc x y z)

  id-left : (x : _) → x ≡ x + zero
```

Furthermore, it needs to be proven that $x + 0 = x$ and $0 + x = x$. The
latter equality is derived automatically by Agda from the definition of
`_+_`{.Agda} - it just computes away. But the first has to be shown,
again by induction:

```
  id-left zero = refl
  id-left (suc x) = ap suc (id-left x)

  mon : _ ⊨ _
```

Then these can be put together to show that the natural numbers model
the theory of monoids. We need to interpret each symbol:

```
  _⊨_.interpretation mon unit x = zero
  _⊨_.interpretation mon ⊗ (x :: y :: nil) = x + y
```

And prove that this interpretation respects the laws:

```
  _⊨_.sound mon associativity (x :: y :: z :: nil) = assoc x y z
  _⊨_.sound mon left-identity vars = refl
  _⊨_.sound mon right-identity (x :: nil) = id-left x
```

# Free monoids (lists)

Another monoid that may be familiar - especially to programmers - is the
type of lists (finite words), with elements drawn from some type $A$. No
matter what $A$ you choose, the type $\mathrm{List}(A)$ has a monoid
structure, with the operation given by `concatenation`{.Agda
ident="_++_"}.

```agda
List-monoid : {ℓ : _} {A : Set ℓ} → List A ⊨ monoid
List-monoid = mon where
  mon : _ ⊨ _
  _⊨_.interpretation mon unit x = nil
  _⊨_.interpretation mon ⊗ (x :: y :: nil) = x ++ y
```

The interpretation of the unit element is given by the empty list,
called `nil`{.Agda}.

```
  _⊨_.sound mon associativity (x :: (x₁ :: (x₂ :: nil))) = ++-assoc x x₁ x₂
  _⊨_.sound mon left-identity vars = refl
  _⊨_.sound mon right-identity (x :: vars) = ++-id-left x
```

The proof that `_++_`{.Agda} is a monoidal operation is constructed in a
different module to save on compilation time. You can click on the
identifiers above to be taken to the proofs, but they are remarkably
similar to the proofs about natural numbers. Indeed, the monoid of
natural numbers under addition is isomorphic to the monoid
$\mathrm{List}(\left\{*\right\})$ of lists on the one-point type.

```
Free : {ℓ : _} → Functor (Sets ℓ) (Mon {ℓ})
```

It can be shown that the `List`{.Agda} type upgrades to the construction
of a functor $\mathrm{Sets} \to \mathrm{Mon}$, the `free monoid`{.Agda
ident=Free} functor.

```
Functor.F₀ Free x = record { model = List x ; models = List-monoid }
```

```
Homomorphism.function (Functor.F₁ Free f) = map f
Homomorphism.homomorphic (Functor.F₁ Free f) unit a = refl
Homomorphism.homomorphic (Functor.F₁ Free f) ⊗ (x :: x₁ :: nil) = map-++ x x₁
```

A function $A \to B$ is taken to a monoid homomorphism $\mathrm{List}(A)
\to \mathrm{List}(B)$ by applying it to each element of the list -
`map`{.Agda}ping it. The proof that $\mathrm{map}(f)$ is a monoid
homomorphism, is, again, in [another ~~castle~~ module].

[another ~~castle~~ module]: agda://1Lab.Data.List

```
Functor.F-id Free = Homomorphism≡ map-id
Functor.F-∘ Free f g = Homomorphism≡ map-comp
```

## The Free ⊣ Forgetful adjunction

A remarkable property is that, writing $U$ for the `Forgetful`{.Agda}
functor $\mathrm{Mon} \to \mathrm{Sets}$, for any set $A$ and monoid
$B$, we have the following isomorphism:

$$
\mathrm{Hom}_{\mathrm{Mon}}(\mathrm{Free}(A), B) \simeq 
\mathrm{Hom}_{\mathrm{Sets}}(A, \mathrm{U}(B))
$$

That is - if $B$ is a monoid, and writing $B$ also for its underlying
set, then the monoid homomorphisms $\mathrm{List}(A) \to B$ do not have
any more information than the plain _functions_ $A \to B$. **This
property characterises an [adjunction]**.

[adjunction]: agda://Category.Functor.Adjoints

However, adjunctions in the 1Lab are more often characterised in terms
of units/counits rather than hom-set isomorphisms. This is because of
our preference for characterisations in terms of [equality of morphisms].

[equality of morphisms]: 1Lab.foundations.html#morphism-equality

```agda
fold : {ℓ : _} → (x : Model {ℓ = ℓ} monoid)
     → List (Model.model x) → Model.model x
```

A small aside is that if we have a list of $A$ where $A$ is some monoid
then we can use the monoid operation to `fold`{.Agda} the list away.
This is a surprise tool that will help us later.

```
fold mod nil = Model.interpretation mod unit nil
fold mod (cons x xs) = Model.interpretation mod ⊗ (x :: (fold mod xs :: nil))
```

```
Free⊣Forget : {ℓ : _} → Free ⊣ Forget {ℓ}
```

The adjunction unit is a natural transformation
$\mathrm{Id}_\mathrm{Sets} \Rightarrow (U \circ \mathrm{Free})$. The right hand
side functor, $(U \circ \mathrm{Free})$, will be abbrevied by
$\mathrm{List}$, since it is the characterisation of the `List`{.Agda}
type as an endofunctor on `Sets`{.Agda}.

This means that we have to invent a function $A \to \mathrm{List}(A)$,
which is [natural] in $A$. The only choice is to take $x$ to the list
containing only $x$.

```
_⊣_.unit Free⊣Forget = NT (λ _ e → cons e nil) λ x y f → refl
```

[natural]: Category.html#natural-transformations

For the adjunction counit, the situation is more complicated, but only
slightly. What we want is a natural transformation $(\mathrm{Free} \circ
U) \Rightarrow \mathrm{Id}_\mathrm{Mon}$, which, abusing notation,
unfolds to asking for a monoid homomorphism $\mathrm{List}(A) \to A$ 

```
η (_⊣_.counit Free⊣Forget) x =
  record { function = fold x
         ; homomorphic = fold-hom
         }
  where
    open Theory monoid
    open _⊨_ (Model.models x)

    _⊕_ : _ → _ → _
    x ⊕ y = interpretation (⊗) (x :: y :: nil)
```

I claim that `fold`{.Agda} is this homomorphism. The complication comes
in proving that `fold`{.Agda} respects `_++_`{.Agda}, which we prove by
equational reasoning.

```
    lemma : (l1 l2 : List (Model.model x))
          → fold x (l1 ++ l2)
          ≡ fold x l1 ⊕ fold x l2
    lemma nil l2           = sound left-identity (_ :: nil)
```

The base case is by the `left-identity`{.Agda} law of the monoid we are
folding into. The inductive step is by the induction hypothesis and
applying the associativity law:

```
    lemma (cons arg l1) l2 =
      arg ⊕ fold x (l1 ++ l2)       ≡⟨ ap (_⊕_ arg) (lemma l1 l2) ⟩
      arg ⊕ (fold x l1 ⊕ fold x l2) ≡⟨ sound associativity (_ :: _ :: _ :: nil) ⟩
      (arg ⊕ fold x l1) ⊕ fold x l2 ∎
```

This lemma readily completes the proof that `fold`{.Agda} is a monoid
homomorphism, thus completing the family of components of the adjunction
counit.

```
    fold-hom : (op : Monoid-operations) (a : Vect (List (Model.model x)) _) → _
    fold-hom unit nil = refl
    fold-hom ⊗ (x :: y :: nil) = lemma x y

is-natural (_⊣_.counit Free⊣Forget) x y f = Homomorphism≡ lemma where
```

We still have to show that `fold` is natural in the monoid we are
folding into, which amounts to showing that, for a monoid homomorphism
$f$, we have

$$
f(\mathrm{fold}(xs)) = \mathrm{fold}(\mathrm{map}(f, xs))
$$

```
  lemma : _
  lemma nil = sym (Homomorphism.homomorphic f _ nil)
  lemma (cons x x₁) rewrite →rewrite(lemma x₁) =
    sym (Homomorphism.homomorphic f _ (_ :: _ :: nil))
```

The zig-zag identities can both be shown by doing case splits and auto.

```
_⊣_.zig Free⊣Forget = Homomorphism≡ lemma where
  lemma : (x : List _) → fold _ (map (λ x → cons x nil) x) ≡ x
  lemma nil = refl
  lemma (cons x xs) = ap (cons _) (lemma xs)

_⊣_.zag Free⊣Forget {B = B} = fun-ext λ x →
  sym (Model.sound B right-identity (x :: nil))
```

---

```agda
module _ {o h : _} (C : Category o h) (a : Category.Ob C) where
  private
    module C = Category.Category C

  endo : Set h
  endo = C.Hom a a
```

# Endomorphism Monoids

Given an object $a$ from a category $C$, the collection of arrows $f : a
\to a$ in $C$ forms a monoid: the _endomorphism monoid_ of $a$. The unit
element is interpreted by the identity hom, and the monoidal operation
is composition.

```
  endo-monoid : endo ⊨ monoid
  _⊨_.interpretation endo-monoid unit x = C.id
  _⊨_.interpretation endo-monoid ⊗ (x :: x₁ :: nil) = x C.∘ x₁
```

This interpretation is sound: The laws of a category are stronger than
the laws of a monoid.

```
  _⊨_.sound endo-monoid associativity (x :: x₁ :: x₂ :: vars) =
    C.assoc₂ x x₁ x₂
  _⊨_.sound endo-monoid left-identity (x :: vars) = sym (C.idl _)
  _⊨_.sound endo-monoid right-identity (x :: vars) = sym (C.idr _)

  Endo : Monoid
  Endo = record { model = endo ; models = endo-monoid }
```

## Deloopings

```
module _ {ℓ ℓ' : _} (M : Monoid {ℓ}) where
  module Mon = Category.Category (Mon {ℓ})
  open Category.Diagrams (Mon {ℓ})
  open Model M

  deloop : Category ℓ' ℓ
```

Up to isomorphism (in the category of monoids), every monoid is an
endomorphism monoid. Specifically, given a monoid `M`{.Agda}, we can
construct a category - its `deloop`{.Agda}ing, denoted $\mathbf{B}M$ - where the endomorphism
monoid of the single object is isomorphic to `M`.

```
  Category.Ob deloop = Top
  Category.Hom deloop tt tt = model
  Category.id deloop = interpretation unit nil
  Category._∘_ deloop m n = interpretation ⊗ (m :: n :: nil)
  Category.idr deloop f = sym (sound right-identity (f :: nil))
  Category.idl deloop f = sym (sound left-identity (f :: nil))
  Category.assoc₁ deloop f g h = sym (sound associativity (f :: g :: h :: nil))
  Category.assoc₂ deloop f g h = sound associativity (f :: g :: h :: nil)
```

In both directions the maps are evident: `Endo deloop tt` is pretty much
definitionally equal to `M`, we just need to pattern match a bit to
expose that.

```
  deloop≈M : M ≈ (Endo deloop tt)
  deloop≈M = into , iso from (Homomorphism≡ λ _ → refl) (Homomorphism≡ λ _ → refl)
    where
      into : Mon.Hom M (Endo deloop tt)
      into =
        record
          { function = λ x → x
          ; homomorphic = λ where
              unit nil → refl
              ⊗ (x :: x₁ :: nil) → refl
          }

      from : Mon.Hom (Endo deloop tt) M
      from =
        record
          { function = λ x → x
          ; homomorphic = λ where
              unit nil → refl
              ⊗ (x :: x₁ :: nil) → refl
          }
```

## The Endomorphism-Delooping adjunction

The construction of `deloop`{.Agda}ing categories extends to a functor
between `Cat*`{.Agda} (the category of pointed categories) and
`Mon`{.Agda}. Similarly, the `Endo`{.Agda}morphism monoid construction 
extends to a functor going the other way. These are adjoints: See
[here](Algebra.Monoid.Endomorphism.html).
