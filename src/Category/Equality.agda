open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Diagrams
open import Category

module Category.Equality where

module _ {o₁ h₁ o₂ h₂} {C : Category o₁ h₁} {D : Category o₂ h₂} where
  -- Characterisation of equality between natural transformations. Since
  -- is-natural _ _ _ is a proposition, it suffices for the family of
  -- maps (η) to be the same.
  NT≡ : {F G : Functor C D} {n1 n2 : F => G}
      → ((x : Category.Ob C) → _=>_.η n1 x ≡! _=>_.η n2 x)
      → n1 ≡! n2
  NT≡ {F} {G} {n1} {n2} etas = go (fun-ext etas) 
    where
      module D = Category.Category D

      go : (p : _=>_.η n1 ≡! _=>_.η n2) → n1 ≡! n2
      go refl = go' (fun-ext λ x → fun-ext λ y → fun-ext λ z → UIP _ _) where
        go' : _=>_.is-natural n1 ≡ _=>_.is-natural n2 → n1 ≡ n2
        go' refl = refl

  -- "Weak" equality of natural transformations, where the domain/range
  -- types are only equal up to two given paths.
  NT≡-weak : {F F' G G' : Functor C D} {n1 : F => G} {n2 : F' => G'}
           → F ≡! F' → G ≡! G'
           → ((x : Category.Ob C) → _=>_.η n1 x ≡ _=>_.η n2 x)
           → n1 ≡ n2
  NT≡-weak refl refl etas = NT≡ etas

-- Characterisation of equality of functors. Two functors are equal when
-- their object parts are equal and when their morphism parts are
-- pointwise equal.
Functor≡ : {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂} {F G : Functor C D}
         → Functor.F₀ F ≡! Functor.F₀ G
         → ({x y : Category.Ob C} (f : Category.Hom C x y) → Functor.F₁ F f ≡ Functor.F₁ G f)
         → F ≡! G
Functor≡ {o₁} {h₁} {o₂} {h₂} {C} {D} {F} {G} refl p = lemma (fun-ext-invis λ x → fun-ext-invis λ y → fun-ext λ f → p f)
  where
    module C = Category.Category C
    module D = Category.Category D

    fmapT : Functor C D → Set _
    fmapT F = {x y : C.Ob} (f : C.Hom x y) → D.Hom (Functor.F₀ F x) (Functor.F₀ F y)

    fidT : Functor C D → Set _
    fidT F = {x : C.Ob} → Functor.F₁ F (C.id {x}) ≡ D.id {Functor.F₀ F x}

    f∘T : Functor C D → Set _
    f∘T F = {x y z : C.Ob} (f : C.Hom y z) (g : C.Hom x y)
          → Functor.F₁ F (f C.∘ g) ≡ Functor.F₁ F f D.∘ Functor.F₁ F g

    p' : _≡_ {_} {A = fmapT F} (Functor.F₁ F) {B = fmapT G} (Functor.F₁ G)
    p' = fun-ext-invis λ x → fun-ext-invis λ y → fun-ext λ f → p f

    lemma : _≡_ {_} {A = fmapT F} (Functor.F₁ F) {B = fmapT G} (Functor.F₁ G) → F ≡ G
    lemma refl = go (fun-ext-invis λ x → UIP _ _) q where
      q = fun-ext-invis λ x → fun-ext-invis λ y → fun-ext-invis λ z → fun-ext λ f → fun-ext λ g → UIP _ _

      go : _≡_ {_} {A = fidT F} (Functor.F-id F) {B = fidT G} (Functor.F-id G)
         → _≡_ {_} {A = f∘T F} (Functor.F-∘ F) {B = f∘T G} (Functor.F-∘ G)
         → F ≡ G
      go refl refl = refl

-- Characterisation of equality for is-Iso.
is-Iso≡ : {o₁ h₁ : _} {C : Category o₁ h₁} {x y : Category.Ob C} {f g : Category.Hom C x y}
        → {i : is-Iso C f} {j : is-Iso C g}
        → f ≡! g
        → is-Iso.g i ≡ is-Iso.g j
        → i ≡ j
is-Iso≡ {i = iso _ linv rinv} {j = iso _ linv₁ rinv₁} refl refl
  rewrite →rewrite (UIP linv linv₁)
  rewrite →rewrite (UIP rinv rinv₁)
  = refl

-- Characterisation of equality for containing a morphism and a proof
-- that they are iso (that is - an isomorphism).
Iso≡ : {o₁ h₁ : _} {C : Category o₁ h₁} {x y : Category.Ob C}
      → {i j : Σ (Category.Hom C x y) (is-Iso C)}
      → Σ.fst i ≡! Σ.fst j
      → is-Iso.g (Σ.snd i) ≡ is-Iso.g (Σ.snd j)
      → i ≡ j
Iso≡ p q = Σ-Path p (is-Iso≡ p q)
