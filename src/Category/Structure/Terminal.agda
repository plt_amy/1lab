open import 1Lab.Base

open import Category

module Category.Structure.Terminal where

record Terminal {o₁ h₁ : _} (C : Category o₁ h₁) : Set (lsuc o₁ ⊔ lsuc h₁) where
  private
    module C = Category.Category C

  field
    ⊤        : C.Ob
    terminal : {A : _} → isContr (C.Hom A ⊤)
  
  ! : {A : _} → C.Hom A ⊤
  ! = isContr.center terminal

  !-unique : {A : _} (f : C.Hom A ⊤) → ! ≡ f
  !-unique = isContr.paths terminal

  !-unique₂ : {A : _} (f : C.Hom A ⊤) (g : C.Hom A ⊤) → f ≡ g
  !-unique₂ f g = sym (isContr.paths terminal f)
             ∘p isContr.paths terminal g
