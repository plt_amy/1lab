open import 1Lab.Base

open import Category.Instances.Cat.Cartesian
open import Category.Structure.Monoidal.Base
open import Category.Structure.Cartesian
open import Category.Instances.Cat.Base
open import Category.Diagrams
open import Category.Equality
open import Category

open Monoidal
open Functor
open _≅_

module Category.Structure.Monoidal.Instances.Cat where

Cat-monoidal : {o h : _} → Monoidal (Cat o h)
-⊗- Cat-monoidal = ×C-Functor Cat-cartesian
Unit Cat-monoidal = Cartesian.⊤ Cat-cartesian
left-unitor Cat-monoidal = r where
  r : _ ≅ _
  to r = NT (λ x → record { F₀ = _×_.snd ; F₁ = _×_.snd ; F-id = refl ; F-∘ = λ f g → refl })
            λ x y f → Functor≡ refl λ _ → refl
  from r = NT (λ x → record { F₀ = λ x → tt , x ; F₁ = λ x → tt , x ; F-id = refl ; F-∘ = λ f g → refl })
              λ x y f → Functor≡ refl λ _ → refl
  to-from r = Functor≡ refl λ _ → refl
  from-to r = Functor≡ refl λ _ → refl
right-unitor Cat-monoidal = r where
  r : _ ≅ _
  to r = NT (λ x → record { F₀ = _×_.fst ; F₁ = _×_.fst ; F-id = refl ; F-∘ = λ f g → refl })
            λ x y f → Functor≡ refl λ _ → refl
  from r = NT (λ x → record { F₀ = λ x → x , tt ; F₁ = λ x → x , tt ; F-id = refl ; F-∘ = λ f g → refl })
              λ x y f → Functor≡ refl λ _ → refl
  to-from r = Functor≡ refl λ _ → refl
  from-to r = Functor≡ refl λ _ → refl
Σ.fst (associator Cat-monoidal) = f where
  f : Functor _ _
  F₀ f (fst , fst₁ , snd) = (fst , fst₁) , snd
  F₁ f (fst , fst₁ , snd) = (fst , fst₁) , snd
  F-id f = refl
  F-∘ f _ _ = refl
Σ.snd (associator Cat-monoidal) = iso g (Functor≡ refl λ _ → refl) (Functor≡ refl λ _ → refl) where
  g : Functor _ _
  F₀ g ((fst , fst₁) , snd) = fst , fst₁ , snd
  F₁ g ((fst , fst₁) , snd) = fst , fst₁ , snd
  F-id g = refl
  F-∘ g _ _ = refl
associator-natural₁ Cat-monoidal = Functor≡ refl λ _ → refl
associator-natural₂ Cat-monoidal = Functor≡ refl λ _ → refl
triangle Cat-monoidal = Functor≡ refl λ _ → refl
pentagon Cat-monoidal = Functor≡ refl λ _ → refl
