open import 1Lab.Base

open import Category

module Category.Slices {o₁ h₁ : _} (C : Category o₁ h₁) where

open Category.Category
private
  module C = Category.Category C

Slice : C.Ob → Category (o₁ ⊔ h₁) h₁
Ob (Slice c) = Σ C.Ob λ a → C.Hom a c
Hom (Slice c) (a , f) (b , g) = Σ (C.Hom a b) λ h → g C.∘ h ≡ f
id (Slice c) = C.id , C.idr _
_∘_ (Slice c) {x} {y} {z} (f , fib) (g , fib₁) = f C.∘ g , p where
  p = Σ.snd z C.∘ f C.∘ g   ≡⟨ C.assoc₂ _ _ _ ⟩
      (Σ.snd z C.∘ f) C.∘ g ≡⟨ ap (λ e → e C.∘ g) fib ⟩
      Σ.snd y C.∘ g         ≡⟨ fib₁ ⟩
      Σ.snd x               ∎
idr (Slice c) {x} {y} f = Σ-Path! (C.idr _) (UIP _ _)
idl (Slice c) {x} {y} f = Σ-Path! (C.idl _) (UIP _ _)
assoc₁ (Slice c) f g h = Σ-Path! (C.assoc₁ _ _ _) (UIP _ _)
assoc₂ (Slice c) f g h = Σ-Path! (C.assoc₂ _ _ _) (UIP _ _)

Coslice : C.Ob → Category (o₁ ⊔ h₁) h₁
Ob (Coslice c) = Σ C.Ob λ a → C.Hom c a
Hom (Coslice c) (a , f) (b , g) = Σ (C.Hom a b) λ h → g ≡ h C.∘ f
id (Coslice c) = C.id , sym (C.idl _)
_∘_ (Coslice c) {x} {y} {z} (f , fib) (g , fib₁) = f C.∘ g , sym p where
  p = (f C.∘ g) C.∘ Σ.snd x ≡⟨ C.assoc₁ f g (Σ.snd x) ∘p ap (C._∘_ f) (sym fib₁) ⟩
      f C.∘ Σ.snd y         ≡⟨ sym fib ⟩
      Σ.snd z               ∎
idr (Coslice c) f = Σ-Path! (C.idr _) (UIP _ _)
idl (Coslice c) f = Σ-Path! (C.idl _) (UIP _ _)
assoc₁ (Coslice c) f g h = Σ-Path! (C.assoc₁ _ _ _) (UIP _ _)
assoc₂ (Coslice c) f g h = Σ-Path! (C.assoc₂ _ _ _) (UIP _ _)
