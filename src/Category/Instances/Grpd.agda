
module Category.Instances.Grpd where

open import Category.Instances.Grpd.Cartesian public
open import Category.Instances.Grpd.Base public
open import Category.Groupoid public
