```agda
open import 1Lab.Base

open import Category

open Category.Category

module Category.Instances.Interval where

ff≤tt : Category lzero lzero
Ob ff≤tt = Bool

Hom ff≤tt tt tt = Top
Hom ff≤tt tt ff = Bot
Hom ff≤tt ff tt = Top
Hom ff≤tt ff ff = Top

id ff≤tt {tt} = tt
id ff≤tt {ff} = tt

_∘_ ff≤tt {tt} {tt} {tt} f g = tt
_∘_ ff≤tt {ff} {tt} {tt} f g = tt
_∘_ ff≤tt {ff} {ff} {tt} f g = tt
_∘_ ff≤tt {ff} {ff} {ff} f g = tt

idr ff≤tt {tt} {tt} _ = refl
idr ff≤tt {ff} {tt} _ = refl
idr ff≤tt {ff} {ff} _ = refl

idl ff≤tt {tt} {tt} _ = refl
idl ff≤tt {ff} {tt} _ = refl
idl ff≤tt {ff} {ff} _ = refl

assoc₁ ff≤tt {tt} {tt} {tt} {tt} f g h = refl
assoc₁ ff≤tt {ff} {tt} {tt} {tt} f g h = refl
assoc₁ ff≤tt {ff} {ff} {tt} {tt} f g h = refl
assoc₁ ff≤tt {ff} {ff} {ff} {tt} f g h = refl
assoc₁ ff≤tt {ff} {ff} {ff} {ff} f g h = refl

assoc₂ ff≤tt {tt} {tt} {tt} {tt} f g h = refl
assoc₂ ff≤tt {ff} {tt} {tt} {tt} f g h = refl
assoc₂ ff≤tt {ff} {ff} {tt} {tt} f g h = refl
assoc₂ ff≤tt {ff} {ff} {ff} {tt} f g h = refl
assoc₂ ff≤tt {ff} {ff} {ff} {ff} f g h = refl
```
