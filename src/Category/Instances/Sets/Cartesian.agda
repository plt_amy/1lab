open import 1Lab.Base

open import Category.Structure.Cartesian
open import Category

module Category.Instances.Sets.Cartesian where

-- products are products ...
Set-products : {o₁ : _} → Cartesian (Sets o₁)
Cartesian._×C_ Set-products A B = A × B
-- ... projections are projections ...
Cartesian.π₁ Set-products = _×_.fst
Cartesian.π₂ Set-products = _×_.snd

-- ... everything else is trivial ...
Cartesian._h×_ Set-products f g x = f x , g x
Cartesian.π₁∘h×≡f Set-products f g = refl
Cartesian.π₂∘h×≡g Set-products f g = refl
Cartesian.h×-unique Set-products refl refl = refl

-- ... the terminal object is the singleton type ...
Cartesian.⊤ Set-products = Top
Cartesian.terminal Set-products = contract (λ x → tt) (λ y → refl)
