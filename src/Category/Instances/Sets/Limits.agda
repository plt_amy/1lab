open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.FinitelyComplete
open import Category

module Category.Instances.Sets.Limits where

-- ... binary pullbacks are subobjects of products ...
Set-finitelyComplete : {o₁ : _} → FinitelyComplete (Sets o₁)
FinitelyComplete.Pullback-of Set-finitelyComplete f g = Σ (_ × _) λ xy → f (_×_.fst xy) ≡! g (_×_.snd xy)
FinitelyComplete.pb-π₁ Set-finitelyComplete ((a , b) , p) = a
FinitelyComplete.pb-π₂ Set-finitelyComplete ((a , b) , p) = b
FinitelyComplete.is-pullback Set-finitelyComplete f g =
  record
  { commutes        = fun-ext λ { (_ , p) → p }
  ; limiting        = λ {Q} {p₁} {p₂} prf q → ((p₁ q , p₂ q) , ap (λ e → e q) prf)
  ; p₁∘limiting≡p₁' = λ eq → refl
  ; p₂∘limiting≡p₂' = λ eq → refl
  ; unique =
      λ {Q} {p₁'} {p₂'} {i} eq fst≡ snd≡ → fun-ext λ q →
        Σ-Path! (×Path (happly fst≡ q) (happly snd≡ q)) (UIP _ _) 
  }

-- ... equalisers are subobjects of the functions' common domain ...
FinitelyComplete.Equaliser-of Set-finitelyComplete f g = Σ _ λ x → f x ≡ g x
FinitelyComplete.equalise Set-finitelyComplete = λ f g x → Σ.fst x
FinitelyComplete.is-equaliser Set-finitelyComplete =
  record
  { commutes      = fun-ext Σ.snd
  ; limiting      = λ {E'} {e'} eq o → (e' o , ap (λ e → e o) eq)
  ; unique        = λ eq x → fun-ext λ x₁ → Σ-Path! (sym (happly x x₁)) (UIP _ _)
  ; e∘limiting≡e' = λ eq → refl 
  }