open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.CartesianClosed
open import Category.Instances.Sets.Cartesian
open import Category

module Category.Instances.Sets.Closed where

Set-exponentials : {o₁ : _} → CartesianClosed (Set-products {o₁})
CartesianClosed.[_,_] Set-exponentials A B = A → B
CartesianClosed.eval Set-exponentials (f , arg) = f arg
CartesianClosed.λf Set-exponentials f a b = f (a , b)
CartesianClosed.β Set-exponentials g = refl
CartesianClosed.λ-unique Set-exponentials h commutes = fun-ext λ a → fun-ext λ b → happly commutes (a , b)