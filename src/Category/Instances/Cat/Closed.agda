open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.CartesianClosed
open import Category.Instances.Cat.Cartesian
open import Category.Instances.FunctorCat
open import Category.Structure.Cartesian
open import Category.Functor.Bifunctor
open import Category.Equality
open import Category

import Category.Associativity

module Category.Instances.Cat.Closed {o h : _} where

open CartesianClosed
open Functor
open _=>_

Cat-closed : CartesianClosed (Cat-cartesian {o ⊔ h} {o ⊔ h})
Cat-closed = rec where
  module Prod = Bifunctor (×C-Functor (Cat-cartesian {o ⊔ h} {o ⊔ h}))

  evl : {A B : _} → Functor (FunctorCat A B ×Cat A) B
  F₀ (evl {A} {B}) (F , x) = F₀ F x
  F₁ (evl {A} {B}) {F , x} {G , y} (nt , f) = η nt _ B.∘ F₁ F f where
    module B = Category.Category B
  F-id (evl {A} {B}) {F , x} = B.idl _ ∘p F-id F where
    module B = Category.Category B
  F-∘ (evl {A} {B}) {F , x} {G , y} {H , z} (nt , f) (nt' , g) =
        (η nt z B.∘ η nt' z) B.∘ F₁ F (f A.∘ g)     ≡⟨ ap (λ e → (η nt _ B.∘ η nt' _) B.∘ e) (F-∘ F f g)  ⟩
        (η nt z B.∘ η nt' z) B.∘ F₁ F f B.∘ F₁ F g  ≡⟨ sym AssB.first2-out-of-4 ∘p AssB.second2-out-of-4 ⟩
        η nt z B.∘ (η nt' z B.∘ F₁ F f) B.∘ F₁ F g  ≡⟨ ap (λ e → η nt _ B.∘ e B.∘ F₁ F _) (is-natural nt' _ _ _) ⟩
        η nt z B.∘ (F₁ G f B.∘ η nt' y) B.∘ F₁ F g  ≡⟨ sym AssB.second2-out-of-4 ∘p AssB.first2-out-of-4 ⟩
        (η nt z B.∘ F₁ G f) B.∘ η nt' y B.∘ F₁ F g  ∎
    where
      module B = Category.Category B
      module AssB = Category.Associativity B
      module A = Category.Category A

  curry : {A B C : _} → Functor (A ×Cat B) C → Functor A (FunctorCat B C)
  F₀ (F₀ (curry F) x) y = F₀ F (x , y)
  F₁ (F₀ (curry {A} {B} F) x) f = F₁ F (Category.id A , f)
  F-id (F₀ (curry F) x) = F-id F
  F-∘ (F₀ (curry {A} {B} {C} F) x) f g =
        F₁ F (A.id , f B.∘ g)               ≡⟨ ap (λ e → F₁ F (e , _)) (sym (A.idl _)) ⟩
        F₁ F (A.id A.∘ A.id , f B.∘ g)      ≡⟨ F-∘ F _ _ ⟩
        F₁ F (A.id , f) C.∘ F₁ F (A.id , g) ∎
    where
      module A = Category.Category A
      module B = Category.Category B
      module C = Category.Category C

  F₁ (curry {A} {B} {C} F) f = NT (λ x₂ → F₁ F (f , B.id)) λ x y g →
        F₁ F (f , B.id) C.∘ F₁ F (A.id , g) ≡⟨ sym (F-∘ F _ _) ⟩
        F₁ F (f A.∘ A.id , B.id B.∘ g)      ≡⟨ ap₂ (λ x y → F₁ F (x , y)) (A.idr f) (B.idl g) ⟩
        F₁ F (f , g)                        ≡⟨ ap₂ (λ x y → F₁ F (x , y)) (sym (A.idl f)) (sym (B.idr g)) ⟩
        F₁ F (A.id A.∘ f , g B.∘ B.id)      ≡⟨ F-∘ F _ _ ⟩
        F₁ F (A.id , g) C.∘ F₁ F (f , B.id) ∎
    where
      module A = Category.Category A
      module B = Category.Category B
      module C = Category.Category C

  F-id (curry F) = NT≡ λ _ → F-id F
  F-∘ (curry {A} {B} {C} F) f g = NT≡ λ _ →
      F₁ F (f A.∘ g , B.id)               ≡⟨ ap (λ e → F₁ F (_ , e)) (sym (B.idl _)) ⟩
      F₁ F (f A.∘ g , B.id B.∘ B.id)      ≡⟨ F-∘ F _ _ ⟩
      F₁ F (f , B.id) C.∘ F₁ F (g , B.id) ∎
    where
      module A = Category.Category A
      module B = Category.Category B
      module C = Category.Category C

  rec : CartesianClosed (Cat-cartesian {o ⊔ h} {o ⊔ h})
  [_,_] rec A B = FunctorCat A B
  eval rec = evl
  λf rec = curry

  β rec {A} {B} {C} F = Functor≡ refl λ where
      {X , f} {Y , g} (fst , snd) →
          F₁ F (fst , B.id) C.∘ F₁ F (A.id , snd) ≡⟨ sym (F-∘ F _ _) ⟩
          F₁ F (fst A.∘ A.id , B.id B.∘ snd)      ≡⟨ ap₂ (λ x y → F₁ F (x , y)) (A.idr _) (B.idl _) ⟩
          F₁ F (fst , snd)                        ∎
    where
      module A = Category.Category A
      module B = Category.Category B
      module C = Category.Category C

  λ-unique rec {A} {B} {C} {g = g} h eval∘h≡g =
    Functor≡ (fun-ext q)
      λ f → NT≡-weak (q _) (q _) λ x → sym (
        F₁ g (f , B.id)                   ≡⟨ sym (r (f , B.id)) ⟩
        η (F₁ h f) x C.∘ F₁ (F₀ h _) B.id ≡⟨ ap (λ e → η (F₁ h f) x C.∘ e) (F-id (F₀ h _)) ⟩
        η (F₁ h f) x C.∘ C.id             ≡⟨ C.idr _ ⟩
        η (F₁ h f) x                      ∎
      )
    where
      module A = Category.Category A
      module B = Category.Category B
      module C = Category.Category C

      r : {a b : _} (f : _ × _)
        → F₁ (evl F∘ Prod.first h) {a} {b} f
        ≡ F₁ g {a} {b} f
      r {a} {b} f = hap (λ e → F₁ e {a} {b} f) eval∘h≡g

      q : (x₁ : _) → F₀ h x₁ ≡! F₀ (λf rec g) x₁
      q x₁ = Functor≡ (fun-ext λ _ → happly (ap F₀ eval∘h≡g) _)
                      λ f → sym (
                          F₁ g (A.id , f)                    ≡⟨ sym (r (Category.id A , f)) ⟩
                          _                                  ≡⟨ is-natural (F₁ h A.id) _ _ _ ⟩
                          F₁ (F₀ h x₁) f C.∘ η (F₁ h A.id) _ ≡⟨ ap (λ e → F₁ (F₀ h x₁) f C.∘ η e _) (F-id h) ⟩
                          F₁ (F₀ h x₁) f C.∘ C.id            ≡⟨ C.idr _ ⟩
                          F₁ (F₀ h x₁) f                     ∎ 
                      )
