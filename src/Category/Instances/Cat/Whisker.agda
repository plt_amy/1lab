open import 1Lab.Base

open import Category.Instances.FunctorCat
open import Category.Instances.Cat.Base
open import Category.Equality
open import Category

module Category.Instances.Cat.Whisker where

open Functor

whisker-L : {o₁ h₁ o₂ h₂ : _}
          → {C : Category o₁ h₁} {D : Category o₂ h₂} {E : Category o₁ h₁}
          → {X Y : Functor C D} (F : Functor D E)
          → X => Y
          → (F F∘ X) => (F F∘ Y)
whisker-L {D = D} {E} {X} {Y} F record { η = η ; is-natural = is-natural } =
  NT (λ x → Functor.F₁ F (η x))
     λ x y f →
      F₁ F (η y) E.∘ F₁ F (F₁ X f)   ≡⟨ sym (Functor.F-∘ F _ _) ⟩
      F₁ F (η y D.∘ F₁ X f)          ≡⟨ ap (F₁ F) (is-natural _ _ _) ⟩
      F₁ F (F₁ Y f D.∘ η x)          ≡⟨ F-∘ F _ _ ⟩
      (F₁ F (F₁ Y f) E.∘ F₁ F (η x)) ∎
  where
    module D = Category.Category D
    module E = Category.Category E

whisker-R : {o₁ h₁ o₂ h₂ o₃ h₃ : _}
          → {C : Category o₁ h₁} {D : Category o₂ h₂} {E : Category o₃ h₃}
          → {X Y : Functor C D} (F : Functor E C)
          → X => Y
          → (X F∘ F) => (Y F∘ F)
whisker-R {D = D} {E} {X} {Y} F record { η = η ; is-natural = is-natural } =
  NT (λ x → η (F₀ F x)) λ x y f → is-natural _ _ _

whisker-L-id : {o₂ h₂ : _}
             → {C D : Category o₂ h₂}
             → {X Y : Functor C D}
             → (nt : X => Y)
             → whisker-L Id nt ≡ nt
whisker-L-id {C = C} {D = D} {X = X} {Y = Y} nt =
  NT≡-weak (Category.idl (Cat _ _) _) (Category.idl (Cat _ _) _) λ x → refl

whisker-R-id : {o₂ h₂ : _}
             → {C : Category o₂ h₂} {D : Category o₂ h₂}
             → {X Y : Functor C D}
             → (nt : X => Y)
             → whisker-R Id nt ≡ nt
whisker-R-id {C = C} {D = D} {X = X} {Y = Y} nt =
  NT≡-weak (Category.idr (Cat _ _) _) (Category.idr (Cat _ _) _) λ x → refl

module _ {o₁ h₁ o₂ h₂ o₃ h₃ : _ }
         {A : Category o₁ h₁} {B : Category o₂ h₂} {C : Category o₃ h₃}
         {F₂ G₂ : Functor B C} {F₁ G₁ : Functor A B} where

  private
    module A = Category.Category A
    module C = Category.Category C
    module B = Category.Category B

    module F₁ = Functor F₁
    module F₂ = Functor F₂
    module G₁ = Functor G₁
    module G₂ = Functor G₂

  -- The Godement product of natural transformations is horizontal
  -- composition of 2-cells in the bicategory Cat.
  -- We can picture this diagramatically as turning this:
  --
  --      ╭── F₁ ─┬─> F₂ ─╮
  --      │   ║   ⋁   ║   ⋁
  --      A   β   B   α   C
  --      │   ⋁   ⋀   ⋁   ⋀
  --      ╰── G₁ ─┴─> G₂ ─╯
  --
  -- into this
  --
  --      ╭── F₁ ∘ F₂ ──╮
  --      │      ║      ⋁
  --      A    β ● α    C
  --      │      ‌‌⋁      ⋀
  --      ╰── G₁ ∘ G₂ ──╯

  _●_ : F₂ => G₂
      → F₁ => G₁
      → (F₂ F∘ F₁) => (G₂ F∘ G₁)
  record { η = β ; is-natural = β-isn } ● record { η = α ; is-natural = α-isn } = NT eta isn where
    eta : _
    eta m = β (Functor.F₀ G₁ m) C.∘ Functor.F₁ F₂ (α m)

    isn : (x y : A.Ob) (f : A.Hom x y)
        → eta y C.∘ Functor.F₁ (F₂ F∘ F₁) f ≡ Functor.F₁ (G₂ F∘ G₁) f C.∘ eta x
    isn x y f =
      (β (G₁.₀ y) C.∘ F₂.₁ (α y)) C.∘ F₂.₁ (F₁.₁ f)  ≡⟨ ap (λ e → e C.∘ _) (β-isn _ _ _) ∘p C.assoc₁ _ _ _ ⟩
      G₂.₁ (α y) C.∘ β (F₁.₀ y) C.∘ F₂.₁ (F₁.₁ f)    ≡⟨ ap (λ e → _ C.∘ e) (β-isn _ _ _) ∘p C.assoc₂ _ _ _ ⟩
      (G₂.₁ (α y) C.∘ G₂.₁ (F₁.₁ f)) C.∘ β (F₁.₀ x)  ≡⟨ ap (λ e → e C.∘ _) (sym (G₂.F-∘ _ _)) ⟩
      G₂.₁ (α y B.∘ F₁.₁ f) C.∘ β (F₁.₀ x)           ≡⟨ ap (λ e → G₂.₁ e C.∘ β (F₁.₀ x)) (α-isn _ _ _) ⟩
      G₂.₁ (G₁.₁ f B.∘ α x) C.∘ β (F₁.₀ x)           ≡⟨ ap (λ e → e C.∘ _) (G₂.F-∘ _ _) ∘p C.assoc₁ _ _ _ ⟩
      G₂.₁ (G₁.₁ f) C.∘ G₂.₁ (α x) C.∘ β (F₁.₀ x)    ≡⟨ ap (C._∘_ _) (sym (β-isn _ _ _)) ⟩
      G₂.₁ (G₁.₁ f) C.∘ β (G₁.₀ x) C.∘ F₂.₁ (α x)    ∎

-F∘_ : {o₁ o₂ o₃ h₁ h₂ h₃ : _} {A : Category o₁ h₁} {B : Category o₂ h₂} {C : Category o₃ h₃}
       (F : Functor A B)
     → Functor (FunctorCat B C) (FunctorCat A C)
Functor.F₀ (-F∘ F) G = G F∘ F
Functor.F₁ (-F∘ F) nt = whisker-R F nt
Functor.F-id (-F∘ F) = refl
Functor.F-∘ (-F∘ F) f g = refl 
