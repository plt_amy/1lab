```
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Instances.Cat.Base
open import Category.Functor.Adjoints
open import Category.Equality
open import Category

module Category.Instances.Cat.Disc where

open Category.Category
open Functor
open _=>_
```

# The object-set functor

```
Ob-functor : {o h : _} → Functor (Cat o h) (Sets o)
```

There is an evident `Functor`{.Agda} which sends a `Category`{.Agda
ident="Category.Category"} to its `set of objects`{.Agda ident=Ob}. It
sends functors to their `object mappings`{.Agda ident="F₀"}.

```
F₀ Ob-functor = Ob
F₁ Ob-functor = F₀
F-id Ob-functor = refl
F-∘ Ob-functor f g = refl
```

# Discrete categories

```
Disc : {ℓ ℓ' : _} → Functor (Sets ℓ) (Cat ℓ (ℓ ⊔ ℓ'))
```

The `discrete category`{.Agda ident=Disc} on a set $C$ has $\mathrm{Ob} = C$ and
only identity morphisms: $\mathrm{Hom}(x, x) = \left\{*\right\}$ and $\mathrm{Hom}(x,
y) = \{\}$. In type theory (with `UIP`{.Agda}), we can represent this by
letting $\mathrm{Hom}(x, y) = x \equiv y$.

```
Ob (F₀ Disc x) = x
```

The type used for the morphism sets is adjusted (`Lift`{.Agda}ed) by a
level `ℓ'`, to allow for more generality.

```
Hom (F₀ (Disc {ℓ' = ℓ'}) x) A B = Lift {ℓ' = ℓ'} (A ≡! B)
id (F₀ Disc x) = lift refl
_∘_ (F₀ Disc x) (lift refl) (lift refl) = lift refl
idr (F₀ Disc x) f = ap lift (UIP _ _)
idl (F₀ Disc x) f = ap lift (UIP _ _)
assoc₁ (F₀ Disc x) f g h = ap lift (UIP _ _)
assoc₂ (F₀ Disc x) f g h = ap lift (UIP _ _)
```

The `discrete category functor`{.Agda ident=Disc} sends a function `f`
to a functor with object part `f`. The morphism part expresses that `f`
preserves equality.

```
F₀ (F₁ Disc f) = f
F₁ (F₁ Disc f) (lift x) = lift (ap f x)
F-id (F₁ Disc x) = ap lift (UIP _ _)
F-∘ (F₁ Disc x) f g = ap lift (UIP _ _)

F-id Disc = Functor≡ refl λ { (lift refl) → refl }
F-∘ Disc f g = Functor≡ refl λ { (lift refl) → refl }
```

## The $\mathrm{Disc} \dashv \mathrm{Ob}$ adjunction

```
open _⊣_

Disc⊣Ob : {o h : _} → Disc {o} {h} ⊣ Ob-functor
```

The `discrete category functor`{.Agda ident=Disc} is a `left adjoint`{.Agda
ident="_⊣_"} to the `object set functor`{.Agda ident="Ob-functor"}. This
means it can be considered a kind of free object: the free category on a set.

```
unit Disc⊣Ob = NT (λ _ x → x) λ x y f → refl
```

The `unit`{.Agda ident=unit} of the adjunction is simple enough. For the
counit, we need to turn a morphism `A → B` in `Disc X` into a morphism
`A → B` in `X`.  Since a morphism `A → B` in `Disc X` is a proof `A ≡
B`, the identity suffices.

```
η (counit Disc⊣Ob) C =
  record
    { F₀ = λ x → x
    ; F₁ = λ { (lift refl) → Category.id C }
    ; F-id = refl
    ; F-∘ = λ { (lift refl) (lift refl) → sym (Category.idr C _) }
    }
is-natural (counit Disc⊣Ob) x y f = Functor≡ refl λ where
  (lift refl) → sym (F-id f)
```

Both zig-zag identities are by pattern matching followed by `refl`{.Agda}.

```
zig Disc⊣Ob = Functor≡ refl λ where
  (lift refl) → refl
zag Disc⊣Ob = refl
```

# Connected Components

Just like in a directed graph, there is a construction which sends a
Category to the set of its path-connected components. This construction
extends to a functor from `Cat`{.Agda} into `Sets`{.Agda}.

```
record Mor {o h : _} (C : Category o h) : Set (o ⊔ h) where
  field
    source : Ob C
    target : Ob C
    map : Hom C source target
```

Two objects are in the same connected component if there is a morphism
connecting them. For this, we need a generic collection of "all
morphisms" - this is `Mor`{.Agda}. 

```
π₀ : {o h : _} → Category o h → Set _
```

The set of connected components is the `coequalizer`{.Agda ident=Coeq}
of the diagram below, which may be more classically described as the
quotient of $\mathrm{Ob}_C$ by the relation which identifies $x \sim y$
iff there exists $f : x \to y$.

~~~{.quiver .short-1}
\[\begin{tikzcd}
  {\mathrm{Mor}_C} && {\mathrm{Ob}_C}
  \arrow["{\mathrm{source}}", shift left=1, from=1-1, to=1-3]
  \arrow["{\mathrm{target}}"', shift right=1, from=1-1, to=1-3]
\end{tikzcd}\]
~~~

```
π₀ C = Coeq (Mor.source {C = C}) Mor.target
```

The `π₀`{.Agda} construction extends to a `Functor`{.Agda} from
`Cat`{.Agda} to `Sets`{.Agda}, where the morphism part, given a functor
$F$, sends the representative of a connected component to the image of
that representative under $F_0$.

```
Conn-comp : {o h : _} → Functor (Cat o h) (Sets _)
F₀ Conn-comp X = π₀ X
F₁ Conn-comp {X} {Y} map π₀X = Coequalizer.elim _ (λ x → coeq (F₀ map x)) resp π₀X where
```

This construction is well-defined: If $x$ and $y$ are in the same
connected component (say, by existence of a morphism $f : x \to y$),
then so are $F_0(x)$ and $F_0(y)$, by existence of $F_1(f) : F_0(x) \to
F_0(y)$.

```
  resp : (y : _) → _
  resp record { source = source ; target = target ; map = hom } =
    substConst ∘p 
      coglue (record { source = _ ; target = _ ; map = F₁ map hom })

F-id Conn-comp =
  fun-ext λ π₀X →
    Coequalizer.elim (λ x → F₁ Conn-comp Id x ≡ x) (λ x → refl) (λ y → UIP _ _) π₀X
F-∘ Conn-comp F G =
  fun-ext λ π₀X →
    Coequalizer.elim
      (λ x → F₁ Conn-comp (F F∘ G) x ≡ F₁ Conn-comp F (F₁ Conn-comp G x))
      (λ x → refl)
      (λ y → UIP _ _)
      π₀X
```

## The $\pi_0 \dashv \mathrm{Disc}$ Adjunction 

The functorial extension of $\pi_0$ extends our adjoint pair
$\mathrm{Disc} \dashv \mathrm{Ob}$ to an adjoint _triple_ $\pi_0 \dashv
\mathrm{Disc} \dashv \mathrm{Ob}$.

```
π₀⊣Disc : {o h : _} → Conn-comp ⊣ Disc {o} {h}
η (unit π₀⊣Disc) X =
  record { F₀ = λ x → coeq x
         ; F₁ = λ x → lift (coglue (record { source = _ ; target = _ ; map = x }))
         ; F-id = ap lift (UIP _ _)
         ; F-∘ = λ f g → ap lift (UIP _ _)
         }
is-natural (unit π₀⊣Disc) x y f = Functor≡ refl λ _ → ap lift (UIP _ _)
```

The adjunction unit is a natural transformation that sends objects to
their respective connected components, and maps to a proof that objects
with maps between them land in the same connected component (recall that
in a discrete category, morphisms are proofs of equality).

```
η (counit π₀⊣Disc) X π₀X = Coequalizer.elim _ (λ x → x) resp π₀X where
  resp : (y : _) → _
  resp record { source = source ; target = .source ; map = (lift refl) } =
    substConst ∘p refl
is-natural (counit π₀⊣Disc) x y f = refl
```

The adjunction counit has to, given a connected component in a discrete
category, pick out a representative. This operation is well-defined
since maps in a discrete category are proofs of equality - by pattern
matching we can prove the choice made is the only possible choice.

The triangle identities follow definitionally.

```
zig π₀⊣Disc = refl
zag π₀⊣Disc = Functor≡ refl λ p → ap lift (UIP _ _)
```

# Codiscrete categories

```
Codisc : {ℓ ℓ' : _} → Functor (Sets ℓ) (Cat ℓ (ℓ ⊔ ℓ'))
```

The codiscrete category on a set $S$ has $\mathrm{Ob} = S$ and
$\mathrm{Hom}(x, y) = \{*\}$.

```
Ob (F₀ Codisc x) = x
Hom (F₀ Codisc x) _ _ = Top
id (F₀ Codisc x) = tt
_∘_ (F₀ Codisc x) tt tt = tt
idr (F₀ Codisc x) tt = refl
idl (F₀ Codisc x) tt = refl
assoc₁ (F₀ Codisc x) tt tt tt = refl
assoc₂ (F₀ Codisc x) tt tt tt = refl
```

The `discrete category functor`{.Agda ident=Disc} sends a function `f`
to a functor with object part `f`. The morphism part is trivial.

```
F₀ (F₁ Codisc f) = f
F₁ (F₁ Codisc f) tt = tt
F-id (F₁ Codisc f) = refl
F-∘ (F₁ Codisc f) _ _ = refl
F-id Codisc = refl
F-∘ Codisc _ _ = refl
```

## The $\mathrm{Ob} \dashv \mathrm{Codisc}$ adjunction

```
Ob⊣Codisc : {o h : _} → Ob-functor ⊣ Codisc {o} {h}
```

The `codiscrete category functor`{.Agda ident=Codisc} is a `right
adjoint`{.Agda ident="_⊣_"}, also to the `object set functor`{.Agda
ident="Ob-functor"}.

```
η (unit Ob⊣Codisc) x =
  record
    { F₀ = λ x → x
    ; F₁ = λ _ → tt
    ; F-id = refl
    ; F-∘ = λ f g → refl
    }
is-natural (unit Ob⊣Codisc) = λ x y f → Functor≡ refl λ _ → refl
```

This time, both the adjunction unit and counit are trivial.

```
counit Ob⊣Codisc = NT (λ _ x → x) λ x y f → refl
zig Ob⊣Codisc = refl
zag Ob⊣Codisc = refl
```
