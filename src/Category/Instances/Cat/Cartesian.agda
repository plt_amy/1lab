open import 1Lab.Base

open import Category.Structure.Cartesian
open import Category.Instances.Cat.Base
open import Category.Structure.Terminal
open import Category.Equality
open import Category

module Category.Instances.Cat.Cartesian where

Cat-cartesian : {o₁ h₁ : _} → Cartesian (Cat o₁ h₁)
Cat-cartesian = r where
  open Functor
  open Cartesian

  r : Cartesian (Cat _ _)
  _×C_ r = _×Cat_
  π₁ r = record { F₀ = _×_.fst ; F₁ = _×_.fst ; F-id = refl ; F-∘ = λ f g → refl }
  π₂ r = record { F₀ = _×_.snd ; F₁ = _×_.snd ; F-id = refl ; F-∘ = λ f g → refl }
  _h×_ r F G =
    record { F₀   = λ x → F₀ F x , F₀ G x
           ; F₁   = λ x → F₁ F x , F₁ G x
           ; F-id = ×Path (F-id F) (F-id G)
           ; F-∘  = λ f g → ×Path (F-∘ F f g) (F-∘ G f g)
           }
  π₁∘h×≡f r f g = Functor≡ refl λ _ → refl
  π₂∘h×≡g r f g = Functor≡ refl λ _ → refl
  h×-unique r refl refl = Functor≡ refl λ _ → refl
  ⊤ r = record
    { Ob = Top
    ; Hom = λ x y → Top
    ; id = tt
    ; _∘_ = λ x x₁ → tt
    ; idr = λ f → refl
    ; idl = λ f → refl
    ; assoc₁ = λ f g h → refl
    ; assoc₂ = λ f g h → refl
    }
  terminal r = contract (record { F₀ = λ x → tt ; F₁ = λ x → tt
                                ; F-id = refl
                                ; F-∘ = λ f g → refl })
                        λ y → Functor≡ refl λ f → refl

open import Category.Slices

Cat* : {o h : _} → Category _ _
Cat* {o} {h} = Coslice (Cat o h) (Terminal.⊤ (Cartesian.is-Terminal Cat-cartesian))
