```
open import 1Lab.Base

open import Category.Equality
open import Category

module Category.Instances.Cat.Base where
```

# The Category of Categories

```
Cat : (o h : _) → Category (lsuc (o ⊔ h)) (o ⊔ h)
```

Fixing two universes `o` and `h`, we can consider the category of all
categories with their type of objects in `o` and their Hom-sets in `h`,
with functors as morphisms.

```
Category.Ob     (Cat o h)       = Category o h
Category.Hom    (Cat o h) C D   = Functor C D
Category.id     (Cat o h)       = Id
Category._∘_    (Cat o h)       = _F∘_
```

The identity and composition are given by the identity functor and
composition of functors. These respect the category laws by definition,
since functors are glorified functors and compute nicely.

```
Category.idr    (Cat o h) F     = Functor≡ refl λ f → refl
Category.idl    (Cat o h) F     = Functor≡ refl λ f → refl
Category.assoc₁ (Cat o h) F G H = Functor≡ refl λ f → refl
Category.assoc₂ (Cat o h) F G H = Functor≡ refl λ f → refl
```

<!--
### What you'll find in `Cat`{.Agda}

Being a category of categories, you'll obviously find as objects of Cat
all the categories that are "`Sets`{.Agda} with fluff": `Sets`{.Agda},
[Mon](agda://Algebra.Monoid#Mon), and more generally all [categories of
models of algebraic theories](agda://Algebra.Theory#_-Models). You'll
also find [the category of finite sets](agda://Topos.Instances.FinSet).

<!--
```
_ = Sets
```
-->

More, you can expect to find the
[discrete](agda://Category.Instances.Cat.Disc#Disc) and
[codiscrete](agda://Category.Instances.Cat.Disc#Codisc) categories on a
Set - the "freely" and "cofreely" generated categories on a Set.

Cat is [Cartesian](agda://Category.Instances.Cat.Cartesian) and
[Cartesian closed](agda://Category.Instances.Cat.Closed), s
-->
