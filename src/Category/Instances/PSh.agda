open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Limit.Continuous.Adjoint
open import Category.Instances.FunctorCat
open import Category.Functor.Properties
open import Category.Functor.Adjoints
open import Category.Limit.Base
open import Category.Equality
open import Category

import Category.Associativity

open Functor
open _=>_

module Category.Instances.PSh {o₁ h₁ : _} (C : Category o₁ h₁) where

PSh : {ℓ : _} → Category _ _
PSh {ℓ} = FunctorCat (C ^op) (Sets (lsuc (o₁ ⊔ ℓ)))

private
  module C = Category.Category C
  module Cop = Category.Category (C ^op)
  module AssC = Category.Associativity C

よ₀ : {ℓ : _} → C.Ob → Functor (C ^op) (Sets (h₁ ⊔ ℓ))
F₀ (よ₀ {ℓ} y) x = Lift {ℓ' = ℓ} (C.Hom x y)
F₁ (よ₀ y) f (lift g) = lift (g C.∘ f)
F-id (よ₀ y) = fun-ext λ where
  (lift f) → ap lift (C.idr f)
F-∘ (よ₀ y) f g = fun-ext λ where
  (lift f) → ap lift AssC.right→left

Embed : {ℓ : _} → Functor C (FunctorCat (C ^op) (Sets (h₁ ⊔ ℓ)))
F₀ (Embed {ℓ}) = よ₀ {ℓ}
η (F₁ Embed f) coord (lift map) = lift (f C.∘ map)
is-natural (F₁ Embed f) x y g = fun-ext λ { (lift p) → ap lift AssC.right→left }
F-id Embed = NT≡ λ x → fun-ext λ { (lift x) → ap lift (C.idl _) }
F-∘ Embed f g = NT≡ λ x → fun-ext λ { (lift x) → ap lift AssC.left→right }

ev : {ℓ : _} (c : C.Ob) → Functor (PSh {ℓ}) (Sets _)
F₀ (ev c) x = F₀ x c
F₁ (ev c) x x₁ = η x c x₁
F-id (ev c) = refl
F-∘ (ev c) f g = refl

const : {ℓ : _} (c : C.Ob) → Functor (Sets ℓ) (FunctorCat (C ^op) (Sets (h₁ ⊔ ℓ)))
F₀ (F₀ (const c) A) d = A × C.Hom d c
F₁ (F₀ (const c) A) f (fst , snd) = fst , snd C.∘ f
F-id (F₀ (const c) A) = fun-ext λ x → ×Path refl (C.idr _)
F-∘ (F₀ (const c) A) f g = fun-ext λ x → ×Path refl (C.assoc₂ _ _ _)
F₁ (const c) f = NT (λ { _ (a , b) → (f a , b) }) λ x y f₁ → refl
F-id (const c) = refl
F-∘ (const c) f g = refl

const⊣ev : (c : C.Ob) → const c ⊣ ev {h₁} c
const⊣ev c = adj where
  open _⊣_
  adj : const c ⊣ ev {h₁} c
  unit adj = NT (λ x x₁ → x₁ , C.id) λ x y f → refl
  counit adj = NT (λ x → NT (λ { _ (a , b) → F₁ x b a }) λ x₁ y f →
    fun-ext λ { (a , b) → happly (F-∘ x _ _) a}) λ x y f → NT≡ λ x₁ → fun-ext λ
    where
      (a , b) → happly (sym (is-natural f _ _ _)) _
  zig adj = NT≡ λ x → fun-ext λ y → ×Path refl (C.idl _)
  zag adj {B} = fun-ext λ x → happly (F-id B) x

ev-continuous : {o₂ h₂ : _} (c : C.Ob) → Continuous {o₂} {h₂} (ev {h₁} c)
ev-continuous c = Radj→Continuous (const⊣ev c)

Embed-fullyFaithful : {ℓ : _} → FullyFaithful (Embed {ℓ})
FullyFaithful.inv Embed-fullyFaithful record { η = η } = Lift.unlift (η _ (lift C.id))
FullyFaithful.linv Embed-fullyFaithful x = C.idr _
FullyFaithful.rinv Embed-fullyFaithful record { is-natural = is-natural ; η = η } =
  NT≡ λ x → fun-ext λ where
    (lift map) → happly (sym (is-natural _ _ _)) _ ∘p ap (λ e → η x (lift e)) (C.idl _)

Embed-faithful : {ℓ : _} → Faithful (Embed {ℓ})
Embed-faithful = FullyFaithful→Faithful Embed-fullyFaithful
