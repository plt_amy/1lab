open import 1Lab.Base

open import Category.Instances.PSh
open import Category

open Category.Category
open _=>_

module Category.Instances.PSh.Quiver where

data Q₀Ob : Set where
  node edge : Q₀Ob

data Q₀Hom : Q₀Ob → Q₀Ob → Set where
  cosource cotarget : Q₀Hom node edge
  idhom : {x : _} → Q₀Hom x x

Q₀ : Category _ _
Q₀ = cat where
  Q₀∘ : {x y z : _} → Q₀Hom y z → Q₀Hom x y → Q₀Hom x z
  Q₀∘ cosource idhom = cosource
  Q₀∘ cotarget idhom = cotarget
  Q₀∘ idhom cosource = cosource
  Q₀∘ idhom cotarget = cotarget
  Q₀∘ idhom idhom = idhom

  cat : Category _ _
  Ob cat = Q₀Ob
  Hom cat = Q₀Hom
  id cat = idhom
  _∘_ cat = Q₀∘

  idr cat cosource = refl
  idr cat cotarget = refl
  idr cat idhom = refl

  idl cat cosource = refl
  idl cat cotarget = refl
  idl cat idhom = refl

  assoc₁ cat cosource idhom idhom = refl
  assoc₁ cat cotarget idhom idhom = refl
  assoc₁ cat idhom cosource idhom = refl
  assoc₁ cat idhom cotarget idhom = refl
  assoc₁ cat idhom idhom cosource = refl
  assoc₁ cat idhom idhom cotarget = refl
  assoc₁ cat idhom idhom idhom  = refl

  assoc₂ cat cosource idhom idhom = refl
  assoc₂ cat cotarget idhom idhom = refl
  assoc₂ cat idhom cosource idhom = refl
  assoc₂ cat idhom cotarget idhom = refl
  assoc₂ cat idhom idhom cosource = refl
  assoc₂ cat idhom idhom cotarget = refl
  assoc₂ cat idhom idhom idhom  = refl

Quiv : {ℓ : _} → Category _ _
Quiv {ℓ} = PSh Q₀ {ℓ = ℓ}

nodes : {ℓ : _} → Category.Ob (Quiv {ℓ}) → Set (lsuc ℓ)
nodes F = Functor.F₀ F node

edges : {ℓ : _} → Category.Ob (Quiv {ℓ}) → Set (lsuc ℓ)
edges F = Functor.F₀ F edge

source : {ℓ : _}
       → (Q : Category.Ob (Quiv {ℓ}))
       → edges Q → nodes Q
source F = Functor.F₁ F cosource

target : {ℓ : _}
       → (Q : Category.Ob (Quiv {ℓ}))
       → edges Q → nodes Q
target F = Functor.F₁ F cotarget

record
  Edges-between
    {ℓ : _} (Q : Category.Ob (Quiv {ℓ}))
    (a : nodes Q) (b : nodes Q)
  : Set (lsuc ℓ)
  where
  
  field
    Edge : edges Q

    right-source : source Q Edge ≡ a
    right-target : target Q Edge ≡ b

Map-edge : {ℓ : _} {Q Q' : Category.Ob (Quiv {ℓ})}
         → (F : Category.Hom (Quiv {ℓ}) Q Q')
         → {a b : _}
         → Edges-between Q a b
         → Edges-between Q' (η F _ a) (η F _ b)
Map-edge F r =
  record { Edge = η F _ (Edge r)
         ; right-source = sym (happly (is-natural F _ _ _) _) ∘p ap (η F node) (right-source r)
         ; right-target = sym (happly (is-natural F _ _ _) _) ∘p ap (η F node) (right-target r)
         }
  where open Edges-between

Edge≡ : {ℓ : _} {Q : Category.Ob (Quiv {ℓ})} {a b : _}
        {E E' : Edges-between Q a b}
      → Edges-between.Edge E ≡ Edges-between.Edge E'
      → E ≡ E'
Edge≡ {E = record { Edge = Edge ; right-source = right-source ; right-target = right-target }}
      {E' = record { Edge = .Edge ; right-source = right-source₁ ; right-target = right-target₁ }}
      refl
  rewrite (→rewrite (UIP right-source right-source₁))
  rewrite (→rewrite (UIP right-target right-target₁))
  = refl
