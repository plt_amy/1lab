```
open import 1Lab.Base

open import Category.Instances.Grpd.Base
open import Category.Instances.Cat.Base
open import Category.Functor.Adjoints
open import Category.Diagrams
open import Category.Equality
open import Category.Groupoid
open import Category

open Functor
open _=>_
open _⊣_

module Category.Instances.Grpd.Core where
```

# Core of a category

```
Core : {o h : _} → Functor (Cat o h) (Grpd o h)
```

The `core`{.Agda ident="Core[_]"} of a category $C$ is the subcategory
of $C$ spanned by the `isomorphisms`{.Agda ident=is-Iso}. Since all of
its morphisms are iso by construction, it is a `groupoid`{.Agda
ident=Groupoid}. The core construction extends to a `functor`{.Agda
ident=Core} from `Cat`{.Agda} to `Grpd`{.Agda}.

```agda
F₀ Core C = Core[ C ]
F₁ Core F =
  record { F₀   = F₀ F
         ; F₁   = λ x → F₁ F (Σ.fst x) , F-iso F (Σ.snd x)
         ; F-id = Iso≡ (F-id F) (F-id F)
         ; F-∘  = λ f g → Iso≡ (F-∘ F _ _) (F-∘ F _ _)
         }
F-id Core = Functor≡ refl λ f → Iso≡ refl refl
F-∘  Core = λ f g → Functor≡ refl λ f → Iso≡ refl refl
```

# The underlying category functor

```
Forget : {o h : _} → Functor (Grpd o h) (Cat o h)
```

There is an evident method of `forgetting`{.Agda ident=Forget} that a
category is a groupoid. This extends to a functor.

```
F₀ Forget = Groupoid.C
F₁ Forget F = F
F-id Forget = refl
F-∘ Forget f g = refl
```

## The $\mathrm{U} \dashv \mathrm{Core}$ adjunction

```
Forget⊣Core : {o h : _} → Forget {o} {h} ⊣ Core
```

The `core functor`{.Agda ident=Core} is right adjoint to the `forgetful
functor`{.Agda ident=Forget} from groupoids to categories. This means
that a core can be seen as a cofree object in `Grpd`{.Agda}.

Construction of the `adjunction unit`{.Agda ident=unit} involves proving
that `¯¹`{.Agda} preserves inverses and flips compositions around, in
addition to a `small lemma`{.Agda ident=lemma} saying that functors take
inverses to inverses.

```
η (unit Forget⊣Core) G =
  record
    { F₀ = λ x → x
    ; F₁ = λ f → f , iso (f ¯¹) (linv _) (rinv _)
    ; F-id = Iso≡ refl id¯¹≡id
    ; F-∘ = λ f g → Iso≡ refl (f∘g¯¹ f g)
    }
  where open Groupoid G
is-natural (unit Forget⊣Core) X Y F =
  Functor≡ refl λ f → Iso≡ refl (sym lemma)
  where
    module X = Groupoid X
    module Y = Groupoid Y

    lemma : {x y : _} {f : X.Hom x y} → F₁ F (f X.¯¹) ≡! (F₁ F f) Y.¯¹
    lemma {f = f} = Y.¯¹-unique _ _ p q where
      p = sym (F-∘ F _ _) ∘p ap (F₁ F) (X.linv f) ∘p F-id F
      q = sym (F-∘ F _ _) ∘p ap (F₁ F) (X.rinv f) ∘p F-id F
```

The `adjunction counit`{.Agda ident=counit} is evident.

```
η (counit Forget⊣Core) C =
  record
    { F₀ = λ x → x
    ; F₁ = λ x → Σ.fst x
    ; F-id = refl
    ; F-∘ = λ f g → refl
    }
is-natural (counit Forget⊣Core) x y f = Functor≡ refl λ f → refl
```

```
zig Forget⊣Core = Functor≡ refl λ f → refl
zag Forget⊣Core = Functor≡ refl λ f → Iso≡ refl refl
```

<!--
```
_ = Groupoid
_ = is-Iso
```
-->
