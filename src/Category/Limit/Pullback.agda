open import 1Lab.Base

open import Category.Structure.Terminal
open import Category.Limit.Base
open import Category.Diagrams
open import Category

module Category.Limit.Pullback where

private
  data cso {ℓ : _} : Set ℓ where
    a b c : cso

  data csh {ℓ ℓ' : _} : cso {ℓ} → cso {ℓ} → Set ℓ' where
    idh : {a : _} → csh a a
    leg₁ : csh a c
    leg₂ : csh b c
  
  comp : {ℓ ℓ' : _} {x y z : _} → csh {ℓ} {ℓ'} y z → csh {ℓ} {ℓ'} x y → csh {ℓ} {ℓ'} x z
  comp idh idh = idh
  comp idh leg₁ = leg₁
  comp idh leg₂ = leg₂
  comp leg₁ idh = leg₁
  comp leg₂ idh = leg₂

Cospan : Category lzero lzero
Cospan = cat where
  open Category.Category

  cat : Category _ _
  Ob cat = cso
  Hom cat = csh
  id cat = idh
  _∘_ cat = comp

  idr cat idh = refl
  idr cat leg₁ = refl
  idr cat leg₂ = refl

  idl cat idh = refl
  idl cat leg₁ = refl
  idl cat leg₂ = refl

  assoc₁ cat idh idh idh = refl
  assoc₁ cat idh idh leg₁ = refl
  assoc₁ cat idh idh leg₂ = refl
  assoc₁ cat idh leg₁ idh = refl
  assoc₁ cat idh leg₂ idh = refl
  assoc₁ cat leg₁ idh idh = refl
  assoc₁ cat leg₂ idh idh = refl

  assoc₂ cat idh idh idh = refl
  assoc₂ cat idh idh leg₁ = refl
  assoc₂ cat idh idh leg₂ = refl
  assoc₂ cat idh leg₁ idh = refl
  assoc₂ cat idh leg₂ idh = refl
  assoc₂ cat leg₁ idh idh = refl
  assoc₂ cat leg₂ idh idh = refl

module _ {o₁ h₁ : _} {C : Category o₁ h₁} where 
  private
    module C = Category.Category C

  Cospan-cone : Functor Cospan C → Set _
  Cospan-cone = Cone {J = Cospan}

  Cospan-limit : Functor Cospan C → Set _
  Cospan-limit = Limit {J = Cospan}

  module _ {F : Functor Cospan C} (l : Limit F) where
    open Terminal l
    open Cone ⊤
    open Pullback
    open Functor

    private
      module F = Functor F
      module L = Cone ⊤

    Cospan-limit→Pullback : Pullback C (ψ a) (ψ b) (F.₁ leg₁) (F.₁ leg₂)
    Cospan-limit→Pullback = pb where
      module _ {Q : C.Ob} {p₁' : C.Hom Q (F.₀ a)} {p₂' : C.Hom Q (F.₀ b)} (eq : F.₁ leg₁ C.∘ p₁' ≡! F.₁ leg₂ C.∘ p₂') where
        Qcone : Cone {J = Cospan} F
        Cone.apex Qcone = Q
        Cone.ψ Qcone a = p₁'
        Cone.ψ Qcone b = p₂'
        Cone.ψ Qcone c = F.₁ leg₁ C.∘ p₁'
        Cone.commutes Qcone idh = ap (λ e → e C.∘ Cone.ψ Qcone _) F.F-id ∘p C.idl _
        Cone.commutes Qcone leg₁ = refl
        Cone.commutes Qcone leg₂ = sym eq

        Qcone=>Lim : Cone=> F Qcone ⊤
        Qcone=>Lim = isContr.center terminal
        
        apex=>→Cone=> : {i : C.Hom Q L.apex} (pi1 : L.ψ a C.∘ i ≡ p₁') (pi2 : L.ψ b C.∘ i ≡ p₂')
                      → Cone=> F Qcone ⊤
        Cone=>.hom (apex=>→Cone=> {i} pi1 pi2) = i
        Cone=>.commutes (apex=>→Cone=> {i} pi1 pi2) {a} = pi1
        Cone=>.commutes (apex=>→Cone=> {i} pi1 pi2) {b} = pi2
        Cone=>.commutes (apex=>→Cone=> {i} pi1 pi2) {c} =
            sym (
                F.₁ leg₁ C.∘ p₁'            ≡⟨ ap (C._∘_ _) (sym pi1) ∘p C.assoc₂ _ _ _ ⟩
                (F.₁ leg₁ C.∘ L.ψ a) C.∘ i  ≡⟨ ap (λ e → e C.∘ i) (L.commutes leg₁) ⟩
                L.ψ c C.∘ i                 ∎
            )

      pb : Pullback C _ _ _ _
      commutes pb = L.commutes leg₁ ∘p sym (L.commutes leg₂)
      limiting pb eq = Cone=>.hom (Qcone=>Lim eq) where
      unique pb eq x x₁ = ap Cone=>.hom (sym (isContr.paths terminal (apex=>→Cone=> eq x x₁)))
      p₁∘limiting≡p₁' pb eq = Cone=>.commutes (Qcone=>Lim eq)
      p₂∘limiting≡p₂' pb eq = Cone=>.commutes (Qcone=>Lim eq)

  module _ {x y z w : _} {f : _} {g : _} {p : _} {q : _} (pb : Pullback C {x} {y} {z} {w} f g p q) where
    open Functor

    Pullback→Diagram : Functor Cospan C
    F₀ Pullback→Diagram a = y
    F₀ Pullback→Diagram b = z
    F₀ Pullback→Diagram c = w
    F₁ Pullback→Diagram {x} {.x} idh = C.id
    F₁ Pullback→Diagram {.a} {.c} leg₁ = p
    F₁ Pullback→Diagram {.b} {.c} leg₂ = q
    F-id Pullback→Diagram = refl
    F-∘ Pullback→Diagram idh idh  = sym (C.idl _)
    F-∘ Pullback→Diagram idh leg₁ = sym (C.idl _)
    F-∘ Pullback→Diagram idh leg₂ = sym (C.idl _)
    F-∘ Pullback→Diagram leg₁ idh = sym (C.idr _)
    F-∘ Pullback→Diagram leg₂ idh = sym (C.idr _)

    Pullback→Limit : Limit Pullback→Diagram
    Terminal.⊤ Pullback→Limit = cone where
      cone : Cone Pullback→Diagram
      Cone.apex cone = x
      Cone.ψ cone a = f
      Cone.ψ cone b = g
      Cone.ψ cone c = p C.∘ f
      Cone.commutes cone idh = C.idl _
      Cone.commutes cone leg₁ = refl
      Cone.commutes cone leg₂ = sym (Pullback.commutes pb)
    Terminal.terminal Pullback→Limit {A} = contr where
      open Cone A

      eq = commutes leg₁ ∘p sym (commutes leg₂)

      hom : Cone=> Pullback→Diagram A _
      Cone=>.hom hom = Pullback.limiting pb eq
      Cone=>.commutes hom {a} = Pullback.p₁∘limiting≡p₁' pb eq
      Cone=>.commutes hom {b} = Pullback.p₂∘limiting≡p₂' pb eq
      Cone=>.commutes hom {c} =
          (_ C.∘ f) C.∘ Pullback.limiting pb eq ≡⟨ ap (λ e → e C.∘ Pullback.limiting pb eq) (Pullback.commutes pb) ∘p C.assoc₁ _ _ _ ⟩
          _                                     ≡⟨ ap (λ e → _ C.∘ e) (Pullback.p₂∘limiting≡p₂' pb eq) ⟩
          _                                     ≡⟨ commutes leg₂ ⟩
          _                                     ∎

      contr : _
      contr = contract hom λ y₁ →
        let c = Cone=>.commutes y₁
        in Cone=>≡ _ (sym (Pullback.unique pb eq (c {a}) c))

open Functor

the : {ℓ : _} (A : Set ℓ) → A → A
the X x = x

Continuous→Pullbacks : {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂} {F : Functor C D}
                     → ({o₃ h₃ : _} → Continuous {o₃} {h₃} F)
                     → {x y z w : _} {f : _} {g : _} {p : _} {q : _}
                     → (pb : Pullback C {x} {y} {z} {w} f g p q)
                     → Pullback D (F₁ F f) (F₁ F g) (F₁ F p) (F₁ F q)
Continuous→Pullbacks {o₁} {h₁} {o₂} {h₂} {C = C} {D = D} {F = F} cont pb = Cospan-limit→Pullback lim' where
  lim' : Limit (F F∘ Pullback→Diagram pb)
  lim' = record { ⊤ = _ ; terminal = cont (Pullback→Limit pb) }

Continuous→Monics : {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂} {F : Functor C D}
                  → ({o₃ h₃ : _} → Continuous {o₃} {h₃} F)
                  → {x y : _} {f : Category.Hom C x y}
                  → is-Mono C f
                  → is-Mono D (F₁ F f)
Continuous→Monics {C = C} {D} {F} cont {f = f} mono = r where
  module C = Category.Category C
  module D = Category.Category D

  pb : Pullback C C.id C.id f f
  pb = Monic→Pullback _ mono

  q : Pullback D (F₁ F C.id) (F₁ F C.id) (F₁ F f) (F₁ F f)
  q = Continuous→Pullbacks {C = C} {D = D} {F = F} cont pb

  r = Pullback→Monic D (subst (λ e → Pullback D e e (F₁ F f) (F₁ F f)) (F-id F) q)
