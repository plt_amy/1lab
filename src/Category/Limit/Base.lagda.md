```agda
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.Terminal
open import Category.Diagrams
open import Category.Equality
open import Category
```

# Idea

The idea of limits generalises many concrete constructions in
mathematics - from several settings, such as set theory, topology and
algebra - to arbitrary categories. A limit, _if it exists_, is "the best
solution" to an "equational problem". For a first intuition, we can
build them graphically, working directly "on top" of a diagram.

## Products

**Note**: Products are described explicitly in
[`Cartesian`](Category.Structure.Cartesian.html). The
description there might be easier to parse.

Consider a _discrete_ diagram - one that has no interesting morphisms,
only identities, such as the collection of objects below.

~~~{.quiver .short-2}
\[\begin{tikzcd}
A & B
\end{tikzcd}\]
~~~

To consider a limit for this diagram, we first have to go through an
intermediate step - a `Cone`{.Agda}. Draw a new object, the
`apex`{.Agda} - let's call it $P$ - and a family of maps from $P$ "down
onto" all of the objects of the diagram. The new maps have to make
everything in sight commute, but in the case of a discrete diagram, we
can pick any maps. There are no obligations.

~~~quiver
\[\begin{tikzcd}
  & P \\
  A && B
  \arrow["\pi_1"', from=1-2, to=2-1]
  \arrow["\pi_2", from=1-2, to=2-3]
\end{tikzcd}\]
~~~

It'll be helpful to think of the maps as projections - which is why
they're labelled with the greek letter $\pi$, for **p**rojection.
However, for an arbitrary cone, the maps are.. well, arbitrary.

To consider a concrete example, we can pretend our diagram was in
$\mathrm{Set}$ all along, and that $A$ was the set $\mathbb{Q}$ and $B$
was the set $\mathbb{R}$. Then the following is a cone over it:

~~~quiver
\[\begin{tikzcd}
  & {\mathbb{N}} \\
  {\mathbb{Q}} && {\mathbb{R}}
  \arrow["{x \mapsto x}"', hook, from=1-2, to=2-1]
  \arrow["{x \mapsto \sqrt{x}}", from=1-2, to=2-3]
\end{tikzcd}\]
~~~

Abstracting again, there is a canonical definition of _cone
homomorphism_ - A map between the apices that makes everything in sight
commute. If $P'$ and $P$ were both apices for our original discrete
diagram, we would draw a cone homomorphism $f : P' \to P$ as the
following commutative, uhm, Starfleet comms badge.

~~~{.quiver .tall-1}
\[\begin{tikzcd}
  & {P'} \\
  & P \\
  A && B
  \arrow["{\pi_1}", from=2-2, to=3-1]
  \arrow["{\pi_2}"', from=2-2, to=3-3]
  \arrow["f", from=1-2, to=2-2]
  \arrow["{\pi_1'}"', curve={height=6pt}, from=1-2, to=3-1]
  \arrow["{\pi_2'}", curve={height=-6pt}, from=1-2, to=3-3]
\end{tikzcd}\]
~~~

These assemble into a category, `Cones`{.agda}, with composition and
identity given by the composition and identity of ambient category. A
`Limit`{.agda} is, then, a [terminal
object](Category.Structure.Terminal.html) in this category. For $P$ to
be a limit in our diagram above, we require that, for any other cone
$P'$ there exists a _unique_ arrow $P' \to P$ that makes everything
commute.

The limit over a discrete diagram is called a **product**, and it's
important to note that the diagram need not be finite. Here are concrete
examples of products in categories:

- In $\mathrm{Sets}$, the limit is the _Cartesian product_ of the objects
of the diagram, and the arrows are the projections onto the factors.

- In $\mathrm{Top}$, the limit is the _product space_ of the objects,
and the arrows are projections, considered as continuous maps. The
product topology can be defined as the coarsest topology that makes the
projections continuous.

- In a partially ordered set, considered as a category, the limit is the
_greatest lower bound_ of the objects - In a poset, we consider that
there exists a map $a \to b$ whenever $a \le b$.

  Normalising the definition of limit slightly, it's works out to be an
  object $p$ such that $p \le a$ and $p \le b$, and if there are any
  other $p'$s satisfying that, then $p' \le p$.

This last example also demonstrates that, while we can always _describe_
the limit, it need not necessarily exist. Consider the poset
$(\mathbb{R} \setminus {0}, \le)$ of real numbers except zero, with the usual
ordering. Then the product indexed by $\{ x \in \mathbb{R} : x > 0 \}$ -
which is normally $0$ - does not exist.

Not every category has every limit. Some categories have no limits at
all! If a category has every limit, it's called _complete_. If it has
all products (including the empty product), we call it
[Cartesian](Category.Structure.Cartesian.html).

## Terminal objects

**Note**: Terminal objects are described explicitly in
[`Terminal`](Category.Structure.Terminal.html). The
description there might be easier to parse.

Perhaps a simpler example of a limit is the one over the empty diagram.
Since the empty diagram is discrete, what we'll end up with is a kind of
product - an empty product. A cone over the empty diagram is an object -
the family of maps, indexed over the objects of the diagram, is empty.

A cone homomorphism works out to be a regular ol' morphism, so the
terminal object in the cone category is a terminal object in the ambient
category! An object $\top$ such that for every other $A$, there's a
unique arrow $A \to \top$.

## Equalisers

**Note**: `Equalisers`{.Agda ident=Equaliser} are described explicitly in
[`Diagrams`](Category.Diagrams.html). The
description there might be easier to parse.

Here's something that happens often in mathematics: Solving an equation.
If both sides of the equation have a free variable - say $x$ - then we
can consider them both as functions of $x$. Here's a silly example I
remember from high school precalculus:

$$
\sin(x) = \cos(x)
$$


Abstracting $x$, we get two arrows $\sin, \cos : \mathbb{R} \to
\mathbb{R}$ in $\mathrm{Sets}$. The solution set of this equation can be
computed explicitly - but it can also be described by a _universal
property_.

A solution set for an equation is a _subset_ of the domain characterised
by the fact that, when the two functions are evaluated at an object in
the subset, they give equal results. Categorically, subsets generalise
into `monomorphisms`{.Agda ident="_↪_"}, and "evaluation" generalises
into composition. So:

~~~{.quiver .short-1}
\[\begin{tikzcd}
  E & {\mathbb{R}} & {\mathbb{R}}
  \arrow[hook, from=1-1, to=1-2]
  \arrow["\sin", shift left=1, from=1-2, to=1-3]
  \arrow["\cos"', shift right=1, from=1-2, to=1-3]
\end{tikzcd}\]
~~~

_The_ solution set $E$, if it exists, is the biggest such set - one
that, if we have another set $E'$ that _equalises_ $\sin$ and $\cos$ -
then $E'$ embeds into $E$.

# Construction

Cones are always considered _over a diagram_ - and by diagram, we mean a
`Functor`{.Agda}, from an _indexing category_ `J`{.Agda}. This category
has no special properties - but in most concrete cases, it'll be a sort
of "shape", like $\bullet \to \bullet \leftarrow \bullet$.

```agda
module Category.Limit.Base where

module _ {o₁ h₁ o₂ h₂ : _} {J : Category o₁ h₁} {C : Category o₂ h₂}
         (F : Functor J C)
       where
  private
    module J = Category.Category J
    module C = Category.Category C
    module F = Functor F
```

## Cones

```
  record Cone : Set (o₁ ⊔ o₂ ⊔ h₁ ⊔ h₂) where
```

A `Cone`{.Agda} is an object (the `apex`{.agda}) together with a
family of maps `ψ`{.Agda ident=ψ} (one for each object in the indexing
category `J`{.Agda}) such that "everything commutes".

```agda
    field
      apex     : C.Ob
      ψ        : (x : J.Ob) → C.Hom apex (F.₀ x)
```

What does it mean for "everything to commute"? For every map $f : x \to
y$ in the indexing category, we require that the diagram below commutes.
This encompasses "everything" since the only compositions that can be
formed with the information at hand are of the form $F(f) \circ
\psi_x$.

~~~{.quiver .short-1}
\[\begin{tikzcd}
  & {\operatorname{apex}} \\
  {F(x)} && {F(y)}
  \arrow["{F(f)}"', from=2-1, to=2-3]
  \arrow["{\psi_x}"', from=1-2, to=2-1]
  \arrow["{\psi_y}", from=1-2, to=2-3]
\end{tikzcd}\]
~~~

```agda
      commutes : {x y : _} (f : J.Hom x y) → F.₁ f C.∘ ψ x ≡! ψ y
```

<details>
<summary>
There is also a characterisation of equality of `Cone`{.Agda}s, which is routine.
</summary>
```agda
  Cone≡ : {x y : Cone}
        → Cone.apex x ≡! Cone.apex y
        → ((o : _) → Cone.ψ x o ≡ Cone.ψ y o)
        → x ≡ y
  Cone≡ {x} {y} refl p = lemma (fun-ext p) where
    lemma : _≡!_ {A = (x : _) → C.Hom (Cone.apex y) (F.₀ x)} (Cone.ψ x) (Cone.ψ y)
          → x ≡! y
    lemma refl = lemma' (fun-ext-invis λ _ → fun-ext-invis λ _ → fun-ext λ f → UIP _ _) where
      lemma' : _≡!_ {A = {a b : _} (f : J.Hom a b) → F.₁ f C.∘ Cone.ψ x a ≡! Cone.ψ x b}
                    (Cone.commutes x) (Cone.commutes y)
             → x ≡ y
      lemma' refl = refl
```
</details>

## Cone homs

```
  record Cone=> (x y : Cone) : Set (o₁ ⊔ h₂) where
```

A `Cone homomorphism`{.Agda ident="Cone=>"} is - like the introduction
says - a map `hom`{.Agda} in the ambient category between the apices,
such that "everything `commutes`{.Agda ident="Cone=>.commutes"}".
Specifically, for any choice of object, the composition of `hom`{.Agda}
with the domain's `ψ`{.Agda} is equal to the codomain's `ψ`{.Agda}.


```agda
    field
      hom      : C.Hom (Cone.apex x) (Cone.apex y)
      commutes : {o : _} → Cone.ψ y o C.∘ hom ≡! Cone.ψ x o
```

<details>
<summary>
There is also an accompanying `characterisation of equality`{.Agda
ident="Cone=>≡"} for cone homomorphisms.
</summary>
```agda
  Cone=>≡ : {x y : Cone} {f g : Cone=> x y} → Cone=>.hom f ≡! Cone=>.hom g → f ≡! g
  Cone=>≡ {x} {y} {f} {g} refl = lemma (fun-ext-invis λ x → UIP _ _) where
    lemma : _≡!_ {A = {o : _} → Cone.ψ y o C.∘ (Cone=>.hom g) ≡! Cone.ψ x o}
                 (Cone=>.commutes f) (Cone=>.commutes g)
          → f ≡! g
    lemma refl = refl
```
</details>

It follows from the equations of the ambient category - namely,
`associativity`{.Agda ident="C.assoc₂"} - that cone homomorphisms
assemble into a category. The definition of `comp`{.Agda} is the
enlightening part, since we have to prove that two cone homomorphisms
again preserve _all_ the commutativities. The identity and associativity
laws for this category are liftings of the underlying category's laws
through `Cone=>≡`{.Agda}, so they're hidden.

```agda
  Cones : Category _ _
  Cones = cat where
    open Category.Category

    comp : {x y z : _} → Cone=> y z → Cone=> x y → Cone=> x z
    comp {x} {y} {z} F G = r where
      open Cone=>
      r : Cone=> x z
      hom r = hom F C.∘ hom G
      commutes r {o} =
          Cone.ψ z o C.∘ hom F C.∘ hom G ≡⟨ C.assoc₂ _ _ _ ∘p ap (λ e → e C.∘ hom G) (commutes F) ⟩
          Cone.ψ y o C.∘ hom G           ≡⟨ commutes G ⟩
          Cone.ψ x o                     ∎
```

<details>
<summary> Final construction of the `category of cones`{.Agda ident=cat} </summary>
```agda
    cat : Category _ _
    Ob cat = Cone
    Hom cat = Cone=>
    id cat = record { hom = C.id ; commutes = C.idr _ }
    _∘_ cat = comp
    idr cat f = Cone=>≡ (C.idr _)
    idl cat f = Cone=>≡ (C.idl _)
    assoc₁ cat f g h = Cone=>≡ (C.assoc₁ _ _ _)
    assoc₂ cat f g h = Cone=>≡ (C.assoc₂ _ _ _)
```
</details>

## Limits

At risk of repeating myself:

> A `Limit`{.agda} is, then, a [terminal
object](Category.Structure.Terminal.html) in this category.

```
  Limit : Set _
  Limit = Terminal Cones

  Limit-apex : Limit → C.Ob
  Limit-apex x = Cone.apex (Terminal.⊤ x)
```

<!--
```agda
module _ {o₁ h₁ o₂ h₂ o₃ h₃ : _}
         {J : Category o₁ h₁}
         {C : Category o₂ h₂}
         {D : Category o₃ h₃}
         {Dia : Functor J C}
         (F : Functor C D)
  where

  private
    module D = Category.Category D
    module C = Category.Category C
    module J = Category.Category J
  
  open Functor
```
-->

# Preservation of limits

A continuous functor is one that preserves limits. More specifically, a
continuous functor is one whose action on cones - `F-map-Cone`{.Agda} -
takes a limiting cone to a limiting cone.

```agda
  F-map-Cone : Cone Dia → Cone (F F∘ Dia)
  Cone.apex (F-map-Cone x) = F₀ F (Cone.apex x)
  Cone.ψ (F-map-Cone x) x₁ = F₁ F (Cone.ψ x x₁)
  Cone.commutes (F-map-Cone x) {y = y} f =
      F₁ F (F₁ Dia f) D.∘ F₁ F (Cone.ψ x _) ≡⟨ sym (F-∘ F _ _) ⟩
      F₁ F (F₁ Dia f C.∘ Cone.ψ x _)        ≡⟨ ap (F₁ F) (Cone.commutes x _) ⟩
      F₁ F (Cone.ψ x y)                     ∎
```

```
  PreservesLimit : Limit Dia → Set _
```

Since limits are only specified up to isomorphism, the definition of
`preservation of limits`{.Agda ident="PreservesLimit"} is specified in
terms of a universal property: A limit $L$ for a digram $\mathrm{Dia}$
is preserved by $F$ the image of $L$ under $F$'s `action on cones`{.Agda
ident="F-map-Cone"} is a terminal object in the category of cones of $F
\circ \mathrm{Dia}$.

```agda
  PreservesLimit o =
      {A : _}
    → isContr (Cone=> (F F∘ Dia) A (F-map-Cone (Terminal.⊤ o)))
```

## Continuity

```agda
Continuous : {o₁ h₁ o₂ h₂ o₃ h₃ : _} {C : Category o₂ h₂} {D : Category o₃ h₃}
           → Functor C D → Set _
```

A continuous functor is one that - for every shape of diagram `J`, and
every diagram `diagram`{.Agda} of shape `J` in `C` - preserves the limit
for that diagram.

```agda
Continuous {o₁} {h₁} {C = C} F
  = {J : Category o₁ h₁} {diagram : Functor J C}
    (L : Limit diagram)
  → PreservesLimit F L
```

<!--
```agda
_ = _↪_
_ = Equaliser
_ = Cone=>.commutes
```
-->

# Uniqueness

```agda
module _ {o₁ h₁ o₂ h₂ : _} {J : Category o₁ h₁} {C : Category o₂ h₂}
         (F : Functor J C)
       where
  private
    module J = Category.Category J
    module C = Category.Category C
    module F = Functor F
    module Cones = Category.Category (Cones F)
```

Above, there has been mention of _the_ limit. The limit of a diagram, if
it exists, is unique up to isomorphism. We prove that here. The argument
is as follows: Fixing a diagram $F$, suppose that $X$ and $Y$ are both
limiting cones for for $F$.

```agda
  Limiting-cone-unique : (X Y : Limit F)
                       → _≈_ (Cones F) (Terminal.⊤ X) (Terminal.⊤ Y)
  Limiting-cone-unique X Y = f , iso g f∘g≡id g∘f≡id where
    X-cone = Terminal.⊤ X
    Y-cone = Terminal.⊤ Y
```

It follows from $Y$ (resp. $X$) being a terminal object that there is a
unique cone homomorphism $f : X \to Y$ (resp $g : Y \to X$).

```
    f : Cones.Hom X-cone Y-cone
    f = Terminal.! Y {X-cone}

    g : Cones.Hom Y-cone X-cone
    g = Terminal.! X {Y-cone}
```

To show that $g$ is an inverse to $f$, consider the composition $g \circ
f$ (the other case is symmetric): It is a map $g \circ f : X \to X$.
Since $X$ is a terminal object, we have that the space of cone
homomorphisms $X \to X$ is contractible - and thus any two such maps are
equal. Thus, $g \circ f = \mathrm{id}_{X} : X \to X$.

```agda
    f∘g≡id : (f Cones.∘ g) ≡! Cones.id
    f∘g≡id = Terminal.!-unique₂ Y (f Cones.∘ g) Cones.id

    g∘f≡id : (g Cones.∘ f) ≡! Cones.id
    g∘f≡id = Terminal.!-unique₂ X (g Cones.∘ f) Cones.id
```

There is an evident functor from `Cones`{.Agda} to `C`, which sends
cones to their apices and cone homomorphisms to their underlying maps.
Being a functor, it preserves isomorphisms, so we get an isomorphism of
the limit _objects_ from the isomorphism of limit _cones_.

<details>
<summary>
Instead of defining a functor, there is a direct proof that an
isomorphism of limits results in an isomorphism of apices. In fact, the
direct proof `Cone≈→apex≈`{.Agda} is what the normal form of defining
the functor and invoking the lemma that functors preserve isomorphisms
would be.
</summary>

```agda
  Cone≈→apex≈ : {X Y : Cone F}
              → _≈_ (Cones F) X Y
              → _≈_ C (Cone.apex X) (Cone.apex Y)
  Cone≈→apex≈ (f , iso g linv rinv) =
          (Cone=>.hom f)
    , iso (Cone=>.hom g)
          (ap Cone=>.hom linv)
          (ap Cone=>.hom rinv)
```
</details>

```
  Limit-unique : {X Y : Limit F}
               → _≈_ C (Cone.apex (Terminal.⊤ X))
                       (Cone.apex (Terminal.⊤ Y))
  Limit-unique {X} {Y} = Cone≈→apex≈ (Limiting-cone-unique X Y)
```

Furthermore, we can do one better: Any isomorphism of limiting cones is
_equal_ to `the one constructed above`{.Agda ident=Limiting-cone-unique}.

```agda
  Limiting-unique² : {X Y : Limit F}
                   → isContr (_≈_ (Cones F) (Terminal.⊤ X) (Terminal.⊤ Y))
  Limiting-unique² {X} {Y} = contract (Limiting-cone-unique X Y) paths where
```

For this, we invoke the `characterisation of equality for
isomorphisms`{.Agda ident="Iso≡"}, which reduces the proof obligation to
showing that any map $X \to Y$ (resp. $Y \to X$) is equal to the one we
exhibited above. Since $Y$ (resp $X$) is a terminal object, this is
immediate:

```agda
    paths : (y : (_≈_ (Cones F) (Terminal.⊤ X) (Terminal.⊤ Y)))
          → Limiting-cone-unique X Y ≡! y
    paths y = Iso≡ (Terminal.!-unique₂ Y _ _) (Terminal.!-unique₂ X _ _)
```
