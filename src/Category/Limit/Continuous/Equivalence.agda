open import Category.Functor.Adjoints.Equivalence
open import Category.Limit.Continuous.Adjoint
open import Category.Limit.Base
open import Category

open Functor

module
  Category.Limit.Continuous.Equivalence
  {o₁ h₁ o₂ h₂ : _}
  {C : Category o₁ h₁} {D : Category o₂ h₂}
  {F : Functor C D}
  (eqv : is-Equiv F)
  where

  Equiv→continuous : {od hd : _} → Continuous {od} {hd} F
  Equiv→continuous = Radj→Continuous (isEquiv→⊢ eqv)
