```
open import 1Lab.Base

open import Category.Functor.NatIso.Properties
open import Category.Functor.Adjoints
open import Category

import Category.Associativity

open Functor
open _=>_

module
  Category.Functor.Adjoints.Equivalence
  where
```

# Adjoint Equivalences

An `adjoint equivalence`{.Agda ident="is-⊣Equiv"} is an
`equivalence of categories`{.Agda ident="is-Equiv"} satisfying `one of
the zig-zag identities`{.Agda ident=zag}. The other identity is also
automatically satisfied, but I am lazy.

```
module 
  _
  {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
  {F : Functor C D}
  where
  private
    module C = Category.Category C
    module AssC = Category.Associativity C
    module D = Category.Category D
    module AssD = Category.Associativity D
    module F = Functor F

  record is-⊣Equiv : Set (o₁ ⊔ o₂ ⊔ h₁ ⊔ h₂) where
    field
      G : Functor D C
    
    module G = Functor G

    field
      unit   : Id ≅ (G F∘ F)
      counit : (F F∘ G) ≅ Id

    module unit = _=>_ (_≅_.to unit)
    module counit = _=>_ (_≅_.to counit)
    
    field
      zag : {B : _} → Functor.F₁ G (counit.η B) C.∘ unit.η (Functor.F₀ G B) ≡! C.id
      zig : {A : _} → (counit.η (F.₀ A)) D.∘ (F.₁ (unit.η A)) ≡! D.id
```

We can not, in general, take the `left inverse`{.Agda ident=linv} to be
the `adjunction counit`{.Agda ident="Adj.counit"}, since that might not
satisfy the `zig`{.Agda ident="Adj.zig"}-`zag`{.Agda ident="Adj.zag"} identities.

```
  Equiv→adjoint : is-Equiv F → is-⊣Equiv
  Equiv→adjoint record { G = G ; linv = linv ; rinv = rinv } = r where
    module linv = _≅_ linv
    module rinv = _≅_ rinv
    module G = Functor G

    ηL→ = η linv.to
    ηL← = η linv.from
    ηR→ = η rinv.to
    ηR← = η rinv.from
```

Fortunately, it's always possible to perturb the left inverse so it
becomes a proper adjunction counit.

```
    linv' : _ ≅ _
    η (_≅_.to linv') x = ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _))
    η (_≅_.from linv') x = F.₁ (G.₁ (ηL→ _)) D.∘ F.₁ (ηR← _) D.∘ ηL← x 
```

<details>
<summary>
The proofs that these two families of morphisms assemble into natural
transformations and are inverses to eachother is routine, but incredibly ugly.
</summary>
```

    is-natural (_≅_.to linv') x y f =
      (ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _))) D.∘ F.₁ (G.₁ f)  ≡⟨ sym AssD.first3-out-of-4 ⟩
      ηL→ y D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _)) D.∘ F.₁ (G.₁ f)    ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ e) (sym (F.F-∘ _ _)) ⟩
      ηL→ y D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _) C.∘ G.₁ f)          ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ F.₁ e) (sym (G.F-∘ _ _)) ⟩
      ηL→ y D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _ D.∘ f))              ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ (F.₁ (G.₁ e))) (is-natural linv.from _ _ _) ⟩
      ηL→ y D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (F.₁ (G.₁ f) D.∘ ηL← x))    ≡⟨ ap (λ e → _ D.∘ e) (sym (F.F-∘ _ _)) ⟩
      ηL→ y D.∘ F.₁ (ηR→ _ C.∘ (G.₁ (F.₁ (G.₁ f) D.∘ ηL← x)))        ≡⟨ ap (λ e → _ D.∘ F.₁ (_ C.∘ e)) (G.F-∘ _ _) ⟩
      ηL→ y D.∘ F.₁ (ηR→ _ C.∘ G.₁ (F.₁ (G.₁ f)) C.∘ G.₁ (ηL← x))    ≡⟨ ap (λ e → _ D.∘ F.₁ e) (C.assoc₂ _ _ _) ⟩
      ηL→ y D.∘ F.₁ ((ηR→ _ C.∘ G.₁ (F.₁ (G.₁ f))) C.∘ G.₁ (ηL← x))  ≡⟨ ap (λ e → ηL→ _ D.∘ F.₁ (e C.∘ G.₁ _)) (is-natural rinv.to _ _ _) ⟩
      ηL→ y D.∘ F.₁ ((G.₁ f C.∘ ηR→ _) C.∘ G.₁ (ηL← x))              ≡⟨ ap (λ e → ηL→ _ D.∘ F.₁ e) (C.assoc₁ _ _ _) ⟩
      ηL→ y D.∘ F.₁ (G.₁ f C.∘ ηR→ _   C.∘ G.₁ (ηL← x))              ≡⟨ ap (λ e → ηL→ _ D.∘ e) (F.F-∘ _ _) ⟩
      ηL→ y D.∘ F.₁ (G.₁ f) D.∘ F.₁ (ηR→ _ C.∘ G.₁ (ηL← x))          ≡⟨ ap (λ e → ηL→ _ D.∘ F.₁ (G.₁ _) D.∘ e) (F.F-∘ _ _) ⟩
      ηL→ y D.∘ F.₁ (G.₁ f) D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← x))    ≡⟨ AssD.first2-out-of-4 ⟩
      (ηL→ _ D.∘ F.₁ (G.₁ f)) D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← x))  ≡⟨ ap (λ e → e D.∘ F.₁ _ D.∘ F.₁ (G.₁ _)) (is-natural linv.to _ _ _) ⟩
      (f D.∘ ηL→ _) D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _))            ≡⟨ sym AssD.first2-out-of-4 ⟩
      f D.∘ ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _))              ∎

    is-natural (_≅_.from linv') x y f =
      (F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (ηR← _) D.∘ ηL← y) D.∘ f                       ≡⟨ sym AssD.first3-out-of-4 ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (ηR← _) D.∘ ηL← y D.∘ f                         ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ F.₁ _ D.∘ e) (is-natural linv.from _ _ _) ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (ηR← _) D.∘ F.₁ (G.₁ f) D.∘ ηL← x               ≡⟨ AssD.second2-out-of-4 ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ (F.₁ (ηR← _) D.∘ F.₁ (G.₁ f)) D.∘ ηL← x             ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ e D.∘ _) (sym (F.F-∘ _ _)) ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (ηR← _ C.∘ G.₁ f) D.∘ ηL← x                     ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ F.₁ e D.∘ _) (is-natural rinv.from _ _ _) ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (G.₁ (F.₁ (G.₁ f)) C.∘ ηR← (G.₀ x)) D.∘ ηL← x   ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ e D.∘ _) (F.F-∘ _ _) ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ (F.₁ (G.₁ (F.₁ (G.₁ f))) D.∘ F.₁ (ηR← _)) D.∘ ηL← x ≡⟨ sym AssD.second2-out-of-4 ⟩
      F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (G.₁ (F.₁ (G.₁ f))) D.∘ F.₁ (ηR← _) D.∘ ηL← x   ≡⟨ AssD.first2-out-of-4 ⟩
      (F.₁ (G.₁ (ηL→ y)) D.∘ F.₁ (G.₁ (F.₁ (G.₁ f)))) D.∘ F.₁ (ηR← _) D.∘ ηL← x ≡⟨ ap (λ e → e D.∘ F.₁ _ D.∘ _) (sym (F.F-∘ _ _)) ⟩
      (F.₁ (G.₁ (ηL→ y) C.∘ G.₁ (F.₁ (G.₁ f)))) D.∘ F.₁ (ηR← _) D.∘ ηL← x       ≡⟨ ap (λ e → F.₁ e D.∘ F.₁ _ D.∘ _ ) (sym (G.F-∘ _ _)) ⟩
      (F.₁ (G.₁ (ηL→ y D.∘ F.₁ (G.₁ f)))) D.∘ F.₁ (ηR← (G.₀ x)) D.∘ ηL← x       ≡⟨ ap (λ e → F.₁ (G.₁ e) D.∘ F.₁ (ηR← _) D.∘ _) (is-natural linv.to _ _ _) ⟩
      (F.₁ (G.₁ (f D.∘ ηL→ x))) D.∘ F.₁ (ηR← (G.₀ x)) D.∘ ηL← x                 ≡⟨ ap (λ e → F.₁ e D.∘ F.₁ (ηR← _) D.∘ _ ) (G.F-∘ _ _) ⟩
      (F.₁ (G.₁ f C.∘ G.₁ (ηL→ x))) D.∘ F.₁ (ηR← (G.₀ x)) D.∘ ηL← x             ≡⟨ ap (λ e → e D.∘ F.₁ _ D.∘ _ ) (F.F-∘ _ _) ⟩
      (F.₁ (G.₁ f) D.∘ F.₁ (G.₁ (ηL→ x))) D.∘ F.₁ (ηR← (G.₀ x)) D.∘ ηL← x       ≡⟨ sym AssD.first2-out-of-4 ⟩
      F.₁ (G.₁ f) D.∘ F.₁ (G.₁ (ηL→ x)) D.∘ F.₁ (ηR← (G.₀ x)) D.∘ ηL← x         ∎
      
    _≅_.to-from linv' {x} =
        (F.₁ (G.₁ (ηL→ x)) D.∘ F.₁ (ηR← (G.F₀ x)) D.∘ ηL← x) D.∘ ηL→ x D.∘ F.₁ (ηR→ (G.F₀ x)) D.∘ F.₁ (G.₁ (ηL← x))
      ≡⟨ sym AssD.first3-out-of-6 ∘p AssD.third2-out-of-6 ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ F.₁ (ηR← (G.F₀ x)) D.∘ (ηL← x D.∘ ηL→ x) D.∘ F.₁ (ηR→ (G.F₀ x)) D.∘ F.₁ (G.₁ (ηL← x))
      ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ F.₁ _ D.∘ e D.∘ F.₁ _ D.∘ F.₁ (G.₁ _)) linv.to-from ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ (F.₁ (ηR← (G.F₀ x)) D.∘ (D.id D.∘ (F.₁ (ηR→ (G.F₀ x)) D.∘ F.₁ (G.₁ (ηL← x)))))
      ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ F.₁ _ D.∘ e) (D.idl _) ∘p AssD.second2-out-of-4 ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ (F.₁ (ηR← (G.F₀ x)) D.∘ F.₁ (ηR→ (G.F₀ x))) D.∘ F.₁ (G.₁ (ηL← x))
      ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ e D.∘ F.₁ (G.₁ _)) (sym (F.F-∘ _ _))  ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ (F.₁ (ηR← (G.F₀ x) C.∘ ηR→ (G.F₀ x))) D.∘ F.₁ (G.₁ (ηL← x))
      ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ F.₁ e D.∘ F.₁ (G.₁ _)) rinv.to-from ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ (F.₁ C.id) D.∘ F.₁ (G.₁ (ηL← x)) ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ e D.∘ F.₁ (G.₁ _)) F.F-id ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ D.id D.∘ F.₁ (G.₁ (ηL← x))       ≡⟨ refl ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ (D.id D.∘ F.₁ (G.₁ (ηL← x)))     ≡⟨ ap (λ e → F.₁ (G.₁ _) D.∘ e) (D.idl _) ⟩
        F.₁ (G.₁ (ηL→ x)) D.∘ F.₁ (G.₁ (ηL← x))                ≡⟨ sym (F.F-∘ _ _) ⟩
        F.₁ (G.₁ (ηL→ x) C.∘ G.₁ (ηL← x))                      ≡⟨ ap F.₁ (sym (G.F-∘ _ _)) ⟩
        F.₁ (G.₁ (ηL→ x D.∘ ηL← x))                            ≡⟨ ap (λ e → F.₁ (G.₁ e)) (linv.from-to) ⟩
        F.₁ (G.₁ D.id)                                         ≡⟨ ap F.₁ G.F-id ⟩
        F.₁ C.id                                               ≡⟨ F.F-id ⟩
        D.id                                                   ∎

    _≅_.from-to linv' {x} =
        (ηL→ x D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← x))) D.∘ F.₁ (G.₁ (ηL→ x)) D.∘ F.₁ (ηR← _) D.∘ ηL← x
      ≡⟨ sym AssD.first3-out-of-6 ∘p AssD.third2-out-of-6 ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ (F.₁ (G.₁ (ηL← x)) D.∘ F.₁ (G.₁ (ηL→ x))) D.∘ F.₁ (ηR← _) D.∘ ηL← x
      ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ e D.∘ F.₁ _ D.∘ _) (sym (F.F-∘ _ _)) ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ (F.₁ (G.₁ (ηL← x) C.∘ (G.₁ (ηL→ x)))) D.∘ F.₁ (ηR← _) D.∘ ηL← x
      ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ F.₁ e D.∘ F.₁ _ D.∘ _) (sym (G.F-∘ _ _)) ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ (F.₁ (G.₁ (ηL← x D.∘ ηL→ x))) D.∘ F.₁ (ηR← _) D.∘ ηL← x
      ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ F.₁ (G.₁ e) D.∘ F.₁ _ D.∘ _) linv.to-from ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ (F.₁ (G.₁ D.id)) D.∘ F.₁ (ηR← _) D.∘ ηL← x ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ F.₁ e D.∘ F.₁ _ D.∘ _) G.F-id ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ (F.₁ C.id) D.∘ F.₁ (ηR← _) D.∘ ηL← x       ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ e D.∘ F.₁ _ D.∘ _) F.F-id ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ D.id D.∘ F.₁ (ηR← _) D.∘ ηL← x             ≡⟨ ap (λ e → _ D.∘ F.₁ _ D.∘ e) (D.idl _) ⟩
        ηL→ x D.∘ F.₁ (ηR→ _) D.∘ F.₁ (ηR← _) D.∘ ηL← x                      ≡⟨ AssD.second2-out-of-4 ∘p ap (λ e → _ D.∘ e D.∘ _) (sym (F.F-∘ _ _)) ⟩
        ηL→ x D.∘ F.₁ (ηR→ _ C.∘ ηR← _) D.∘ ηL← x                            ≡⟨ ap (λ e → _ D.∘ F.₁ e D.∘ _) (rinv.from-to) ⟩
        ηL→ x D.∘ F.₁ C.id D.∘ ηL← x                                         ≡⟨ ap (λ e → _ D.∘ e D.∘ _) F.F-id ⟩
        ηL→ x D.∘ D.id D.∘ ηL← x                                             ≡⟨ ap (D._∘_ _) (D.idl _) ⟩
        ηL→ x D.∘ ηL← x                                                      ≡⟨ linv.from-to ⟩
        D.id                                                                 ∎
```

</details>

```
    eqv-zag : {B : D.Ob}
            →   G.₁ (ηL→ B D.∘ F.₁ (ηR→ (G.F₀ B))
            D.∘ F.₁ (G.₁ (ηL← B))) C.∘ ηR← (G.F₀ B)
            ≡!  C.id

    eqv-zig : {A : C.Ob}
            →   (ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _)))
            D.∘ F.₁ (ηR← A)
           ≡! D.id
```

<details>
<summary> These aren't much better! </summary>
```
    eqv-zag {B} =
        G.₁ (ηL→ B D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← B))) C.∘ ηR← (G.F₀ B)        ≡⟨ ap (λ e → e C.∘ _) (G.F-∘ _ _) ∘p sym AssC.right→left ⟩
        G.₁ (ηL→ B) C.∘ G.₁ (F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← B))) C.∘ ηR← (G.F₀ B)  ≡⟨ ap (λ e → G.₁ _ C.∘ e C.∘ _) (G.F-∘ _ _) ∘p sym AssC.second2-out-of-4 ⟩
        G.₁ (ηL→ B) C.∘ G.₁ (F.₁ (ηR→ _)) C.∘ G.₁ (F.₁ (G.₁ (ηL← B))) C.∘ ηR← _   ≡⟨ ap (λ e → G.₁ _ C.∘ G.₁ (F.₁ _) C.∘ e) (sym (is-natural rinv.from _ _ _)) ⟩ 
        G.₁ (ηL→ B) C.∘ G.₁ (F.₁ (ηR→ _)) C.∘ ηR← (G.F₀ _) C.∘ G.₁ (ηL← B)        ≡⟨ AssC.second2-out-of-4 ⟩
        G.₁ (ηL→ B) C.∘ (G.₁ (F.₁ (ηR→ _)) C.∘ ηR← (G.F₀ _)) C.∘ G.₁ (ηL← B)      ≡⟨ ap (λ e → G.₁ _ C.∘ e C.∘ G.₁ _) (sym (is-natural rinv.from _ _ _)) ⟩
        G.₁ (ηL→ B) C.∘ (ηR← (G.₀ B) C.∘ ηR→ (G.₀ B)) C.∘ G.₁ (ηL← B)             ≡⟨ ap (λ e → G.₁ _ C.∘ e C.∘ G.₁ _) rinv.to-from ⟩
        G.₁ (ηL→ B) C.∘ C.id C.∘ G.₁ (ηL← B)                                      ≡⟨ ap (C._∘_ (G.₁ _)) (C.idl _) ⟩
        G.₁ (ηL→ B) C.∘ G.₁ (ηL← B)                                               ≡⟨ sym (G.F-∘ _ _) ⟩
        G.₁ (ηL→ B D.∘ ηL← B)                                                     ≡⟨ ap G.₁ linv.from-to ⟩
        G.₁ D.id                                                                  ≡⟨ G.F-id ⟩
        C.id                                                                      ∎

    eqv-zig {A} =
        (ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ F.₁ (G.₁ (ηL← _))) D.∘ F.₁ (ηR← A)   ≡⟨ ap (λ e → (_ D.∘ F.₁ _ D.∘ e) D.∘ F.₁ _) (sym (F≃id-natural₂ linv)) ⟩
        (ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ ηL← _) D.∘ F.₁ (ηR← A)               ≡⟨ sym AssD.first3-out-of-4 ⟩
        ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ ηL← _ D.∘ F.₁ (ηR← A)                 ≡⟨ ap (λ e → ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ e) (is-natural linv.from _ _ _) ⟩
        ηL→ _ D.∘ F.F₁ (ηR→ _) D.∘ F.F₁ (G.F₁ (F.F₁ (ηR← A))) D.∘ ηL← _ ≡⟨ ap (λ e → ηL→ _ D.∘ F.₁ (ηR→ _) D.∘ F.₁ e D.∘ ηL← _) (sym (F≃id-natural₂ rinv)) ⟩
        ηL→ _ D.∘ F.F₁ (ηR→ _) D.∘ F.F₁ (ηR← _) D.∘ ηL← _               ≡⟨ AssD.second2-out-of-4 ⟩
        ηL→ _ D.∘ (F.F₁ (ηR→ _) D.∘ F.F₁ (ηR← _)) D.∘ ηL← _             ≡⟨ ap (λ e → ηL→ _ D.∘ e D.∘ ηL← _) (sym (F.F-∘ _ _)) ⟩
        ηL→ _ D.∘ (F.F₁ (ηR→ _ C.∘ ηR← _)) D.∘ ηL← _                    ≡⟨ ap (λ e → ηL→ _ D.∘ F.₁ e D.∘ ηL← _) rinv.from-to ⟩
        ηL→ _ D.∘ F.₁ C.id D.∘ ηL← _                                    ≡⟨ ap (λ e → ηL→ _ D.∘ e D.∘ ηL← _) F.F-id ⟩
        ηL→ _ D.∘ D.id D.∘ ηL← _                                        ≡⟨ ap (D._∘_ (ηL→ _)) (D.idl _) ⟩
        ηL→ _ D.∘ ηL← _                                                 ≡⟨ linv.from-to ⟩
        D.id                                                            ∎
```
</details>

Also need to make sure the `adjunction unit`{.Agda ident=unit} points in
the right direction!

```
    rinv' = record { to = _≅_.from rinv
                   ; from = _≅_.to rinv
                   ; to-from = _≅_.from-to rinv
                   ; from-to = _≅_.to-from rinv
                   }

    r : is-⊣Equiv
    r = record  
        { G      = G
        ; unit   = rinv'
        ; counit = linv'
        ; zag    = eqv-zag
        ; zig    = eqv-zig
        }
```

# All equivalences are adjoints

Repackaging `Equiv→adjoint`{.Agda} slightly, we get that a functor that is
an equivalence of categories is both a left and right `adjoint`{.Agda
ident="_⊣_"}.

```
isEquiv→⊣ : {o₁ h₁ o₂ h₂ : _}
             {C : Category o₁ h₁} {D : Category o₂ h₂}
             {F : Functor C D}
           → (e : is-Equiv F)
           → F ⊣ is-Equiv.G e
isEquiv→⊣ e =
  record { unit = _≅_.to unit ; counit = _≅_.to counit ; zag = zag ; zig = zig }
  where open is-⊣Equiv (Equiv→adjoint e)

isEquiv→⊢ : {o₁ h₁ o₂ h₂ : _}
             {C : Category o₁ h₁} {D : Category o₂ h₂}
             {F : Functor C D}
           → (e : is-Equiv F)
           → is-Equiv.G e ⊣ F
isEquiv→⊢ {F = F} e =
  record { unit = _≅_.to unit ; counit = _≅_.to counit ; zag = zag ; zig = zig }
  where
    open is-Equiv e

    p : is-Equiv (is-Equiv.G e)
    p = record { G = F ; linv = rinv ; rinv = linv }

    open is-⊣Equiv (Equiv→adjoint p)
```

<!--
```
module Adj = Category.Functor.Adjoints._⊣_
_ = Adj.counit
_ = Adj.zig
_ = Adj.zag
```
-->
