```
open import 1Lab.Base

open import Category.Constructions.Monad
open import Category.Functor.Adjoints
open import Category

open Functor
open Monad
open _=>_
```

# The Monad from an Adjunction

```
module
  Category.Functor.Adjoints.Monad
  {o₁ h₁ o₂ h₂ : _}
  {C : Category o₁ h₁}
  {D : Category o₂ h₂}
  {L : Functor C D} {R : Functor D C}
  (L⊣R : L ⊣ R)
  where

private
  module C = Category.Category C
  module D = Category.Category D
  module L = Functor L
  module R = Functor R
  module adj = _⊣_ L⊣R
```

Every adjunction $L \dashv R$ gives rise to a monad, where the
underlying functor is $R \circ L$.

```
Adjunction→Monad : Monad
M Adjunction→Monad = R F∘ L
```

The unit of the monad is just adjunction monad, and the multiplication
comes from the counit.

```
unit Adjunction→Monad = adj.unit
mult Adjunction→Monad = NT (λ x → R.₁ (η adj.counit (L.₀ x))) λ x y f →
  R.₁ (adj.counit.η (L.₀ y)) C.∘ R.₁ (L.₁ (R.₁ (L.₁ f))) ≡⟨ sym (R.F-∘ _ _) ⟩
  R.₁ (adj.counit.η (L.₀ y) D.∘ L.₁ (R.₁ (L.₁ f)))       ≡⟨ ap R.₁ (adj.counit.is-natural _ _ _) ⟩
  R.₁ (L.₁ f D.∘ adj.counit.η (L.₀ x))                   ≡⟨ R.F-∘ _ _ ⟩
  _                                                      ∎
```

The monad laws follow from the zig-zag identities. In fact, the
`right-ident`{.Agda}ity law is exactly the `zag`{.Agda ident="adj.zag"}
identity.

```
right-ident Adjunction→Monad {x} = adj.zag
```

The others are slightly more involved.

```
left-ident Adjunction→Monad =
  R.₁ (adj.counit.η _) C.∘ R.₁ (L.₁ (adj.unit.η _)) ≡⟨ sym (R.F-∘ _ _) ⟩
  R.₁ (adj.counit.η _ D.∘ L.₁ (adj.unit.η _))       ≡⟨ ap R.₁ adj.zig ⟩
  R.₁ D.id                                          ≡⟨ R.F-id ⟩
  C.id                                              ∎
assoc Adjunction→Monad =
  R.₁ (adj.counit.η _) C.∘ R.₁ (L.₁ (R.₁ (adj.counit.η _)))   ≡⟨ sym (R.F-∘ _ _) ⟩
  R.₁ (adj.counit.η _ D.∘ L.₁ (R.₁ (adj.counit.η _)))         ≡⟨ ap R.₁ (adj.counit.is-natural _ _ _) ⟩
  R.₁ (adj.counit.η _ D.∘ adj.counit.η _)                     ≡⟨ R.F-∘ _ _ ⟩
  R.₁ (adj.counit.η _) C.∘ R.₁ (adj.counit.η _)               ∎
```
