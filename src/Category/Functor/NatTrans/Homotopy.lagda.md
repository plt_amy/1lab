```
open import 1Lab.Isomorphism
open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Instances.Interval
open import Category.Functor.Bifunctor
open import Category.Equality
open import Category

open Functor
open _=>_

module
  Category.Functor.NatTrans.Homotopy
  {o₁ h₁ o₂ h₂ : _}
  {C : Category o₁ h₁} {D : Category o₂ h₂}
  where

private
  module C = Category.Category C
  module D = Category.Category D
```

# Homotopies between Functors

```
record _∼_ (F G : Functor C D) : Set (o₁ ⊔ o₂ ⊔ h₁ ⊔ h₂) where
  field
    Paths : Functor (ff≤tt ×Cat C) D

  module Paths = Bifunctor Paths
```

A `homotopy`{.Agda ident="_∼_"} between `Functors`{.Agda
ident=Functor} is a functor $\left\{0\le1\right\} × C \to D$. Since
`Cat`{.Agda ident=Cat} is [Cartesian closed](agda://Category.Instances.Cat.Closed),
this is equivalently a functor from [the interval
category](agda://Category.Instances.Interval) to
[`[C,D]`](agda://Category.Instances.FunctorCat#FunctorCat) - a _path_ in `[C,D]`.

```
  field
    left-endpoint : Paths.Right ff ≡ F
    right-endpoint : Paths.Right tt ≡ G
```

A homotopy $H : F \Rightarrow G$ additionally carries around information
witnessing that the `left`{.Agda ident=left-endpoint} $H(0, -)$ and
`right`{.Agda ident=right-endpoint} $H(1, -)$ endpoints match $F$ and
$G$ respectively. The data of a homotopy is exactly the data of a
natural transformation:

<details>
<summary> Aside: Characterisation of equality for `Homotopy`{.Agda} </summary>

Homotopies are equal when their underlying functors are equal; The
proofs that the endpoints match are immaterial.

```
Homotopy≡ : {F G : Functor C D} {H H' : F ∼ G}
          → _∼_.Paths H ≡ _∼_.Paths H'
          → H ≡ H'
Homotopy≡ {H  = record { Paths = Paths ; left-endpoint = le ; right-endpoint = re }}
          {H' = record { Paths = .Paths ; left-endpoint = le' ; right-endpoint = re' }}
          refl
  rewrite →rewrite (UIP le le')
  rewrite →rewrite (UIP re re') = refl
```
</details>

## Natural transformation to homotopy

The proof that a `natural transformation`{.Agda ident="_=>_"} determines
a `homotopy`{.Agda ident="_∼_"} is by exhaustion.

```
NatTrans→Homotopy : {F G : Functor C D} → F => G → F ∼ G
NatTrans→Homotopy {F} {G} nt =
  record
    { Paths          = Func
    ; left-endpoint  = Functor≡ refl λ f → refl
    ; right-endpoint = Functor≡ refl λ f → refl
    }
  where

    Func : Functor _ _
    F₀ Func (ff , snd) = F₀ F snd
    F₀ Func (tt , snd) = F₀ G snd
```

We construct `a functor`{.Agda ident=Func} that, for objects:

- Sends $(0, x)$ to $F(x)$
- Sends $(1, x)$ to $G(x)$

```
    F₁ Func {ff , _} {ff , _} (tt , fun) = F₁ F fun
    F₁ Func {ff , _} {tt , _} (tt , fun) = F₁ G fun D.∘ η nt _
    F₁ Func {tt , _} {tt , _} (tt , fun) = F₁ G fun
```

And for morphisms:

- Sends $(*_{0 \to 0}, f)$ to $F(f)$
- Sends $(*_{1 \to 1}, f)$ to $G(f)$
- Sends $(*_{0 \to 1}, f)$ to $G(f) \circ \eta$, where $\eta$ is the
natural transformation. This is the "space in between" the endpoints,
where we use the natural transformation to "continuously transform" F
into G.


```
    F-id Func {ff , snd} = F-id F
    F-id Func {tt , snd} = F-id G

    F-∘ Func {ff , _} {ff , _} {ff , _} f g = F-∘ F _ _
    F-∘ Func {tt , _} {tt , _} {tt , _} f g = F-∘ G _ _

    F-∘ Func {ff , _} {tt , _} {tt , _} (tt , f) (tt , g) =
      F₁ G (f C.∘ g) D.∘ η nt _      ≡⟨ ap (λ e → e D.∘ _) (F-∘ G _ _) ⟩
      (F₁ G f D.∘ F₁ G g) D.∘ η nt _ ≡⟨ D.assoc₁ _ _ _ ⟩
      F₁ G f D.∘ F₁ G g D.∘ η nt _   ∎
```

The proof that this construction determines a functor is routine case
analysis + equational reasoning.

```
    F-∘ Func {ff , _} {ff , _} {tt , _} (tt , f) (tt , g) =
      F₁ G (f C.∘ g) D.∘ η nt _      ≡⟨ ap (λ e → e D.∘ _) (F-∘ G _ _) ⟩
      (F₁ G f D.∘ F₁ G g) D.∘ η nt _ ≡⟨ D.assoc₁ _ _ _ ⟩
      F₁ G f D.∘ F₁ G g D.∘ η nt _   ≡⟨ ap (D._∘_ _) (sym (is-natural nt _ _ _)) ⟩
      F₁ G f D.∘ η nt _ D.∘ F₁ F g   ≡⟨ D.assoc₂ _ _ _ ⟩
      (F₁ G f D.∘ η nt _) D.∘ F₁ F g ∎
```

The interesting case is the one where we apply the assumption of
`naturality`{.Agda ident=is-natural}.

## Homotopy to Natural transformation

```
Homotopy→NatTrans : {F G : Functor C D} → F ∼ G → F => G
Homotopy→NatTrans {F} {G}
  record{ Paths = Paths
        ; left-endpoint = le
        ; right-endpoint = re
        } = nt' le re
  where
  module Paths = Bifunctor Paths
```

The proof that a `homotopy`{.Agda ident="_∼_"} determines a `natural
transformation`{.Agda ident="=>"} is much simpler.

```
  nt' : (le : Paths.Right ff ≡ F) (re : Paths.Right tt ≡ G) → F => G
  η (nt' le re) x =
    transport (ap₂ D.Hom (hap (λ e → F₀ e x) le) (hap (λ e → F₀ e x) re))
              (F₁ Paths (tt , C.id {x}))
```

We can directly determine a family of maps $\mathrm{Paths}(0, x) \to
\mathrm{Paths}(1, x)$ using the functorial action of
$\mathrm{Paths}(*_{0 \to 1}, -)$. The only complication is the endpoint
adjustment that must be done to turn $\mathrm{Paths}(0, x)$ to $F(x)$
(resp. for $1$ and $G(x)$.)

```
  is-natural (nt' refl refl) x y f =
    F₁ Paths (tt , C.id) D.∘ F₁ Paths (tt , f) ≡⟨ sym (F-∘ Paths _ _) ⟩
    F₁ Paths (tt , C.id C.∘ f)                 ≡⟨ ap (λ e → F₁ Paths (_ , e)) (C.idl _ ∘p sym (C.idr _)) ⟩
    F₁ Paths (tt , f C.∘ C.id)                 ≡⟨ F-∘ Paths _ _ ⟩
    F₁ Paths (tt , f) D.∘ F₁ Paths (tt , C.id) ∎
```

This is natural by a direct equational reasoning argument.


```
NatTrans≃Homotopy : {F G : Functor C D} → (F => G) ≃ F ∼ G
NatTrans≃Homotopy {F} {G} = iso NatTrans→Homotopy Homotopy→NatTrans p q where
```

<details>
<summary>
These functions can be shown to be inverses, by, again, annoying case
analysis and equational reasoning arguments.
</summary>

```
  p : (x : F => G) → _
  p (NT η₁ is-natural₁) = NT≡ λ x →
    transport _ (F₁ G C.id D.∘ η₁ x) ≡⟨ sym (transportConst _ (F₁ G C.id D.∘ η₁ x)) ⟩
    F₁ G C.id D.∘ η₁ x               ≡⟨ ap (λ e → e D.∘ _) (F-id G) ⟩
    D.id D.∘ η₁ x                    ≡⟨ D.idl _ ⟩
    η₁ x                             ∎

  q : (x : F ∼ G) → _
  q record { Paths = record { F₀ = F₀ ; F₁ = F₁ ; F-id = F-id ; F-∘ = F-∘ }
           ; left-endpoint = refl
           ; right-endpoint = refl
           }
    = Homotopy≡ (Functor≡ (fun-ext λ { (tt , snd) → refl
                                     ; (ff , snd) → refl })
                          λ { {tt , _} {tt , _} f → refl
                            ; {ff , _} {tt , _} (_ , f) →
                              F₁ (tt , f) D.∘ F₁ (tt , C.id) ≡⟨ sym (F-∘ _ _) ⟩
                              F₁ (tt , f C.∘ C.id)           ≡⟨ ap (λ e → F₁ (tt , e)) (C.idr _) ⟩
                              F₁ (tt , f)                    ∎
                            ; {ff , _} {ff , _} f → refl })
```
</details>
