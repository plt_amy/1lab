```agda
open import 1Lab.Function.Properties
open import 1Lab.HProp
open import 1Lab.Base

open import Category

open Functor

module
  Category.Functor.Properties
  {o₁ h₁ o₂ h₂ : _} {C : Category o₁ h₁} {D : Category o₂ h₂}
  where
```

# Properties of functors

```
private
  module C = Category.Category C
  module D = Category.Category D
```

```
Full : Functor C D → Set _
```

A _full functor_ is one for which the morphism mapping is surjective.

```
Full F = {x y : C.Ob} → surjection (F₁ F {x} {y})
```

---

```
Faithful : Functor C D → Set _
```

A _faithful functor_ is one for which the morphism mapping is injective.

```
Faithful F = {x y : C.Ob} → injection (F₁ F {x} {y})
```

---

```
record FullyFaithful (F : Functor C D) : Set (o₁ ⊔ h₁ ⊔ h₂) where
```

A _fully faithful functor_ (ff functor) is one that is both full
(surjective) and faithful (injective). Functions between sets are
bijective iff they are injective and surjective, and they are bijective
iff they have inverses, so the definition of ff functor just says that
the morphism mapping has an inverse.

```
  constructor ff
  field
    inv  : {x y : C.Ob} → D.Hom (F₀ F x) (F₀ F y)     → C.Hom x y
    linv : {a b : C.Ob} (x : C.Hom a b)               → inv (F₁ F x) ≡! x
    rinv : {a b : C.Ob} (x : D.Hom (F₀ F a) (F₀ F b)) → F₁ F (inv x) ≡! x
```


```
FullyFaithful→Faithful : {F : Functor C D} → FullyFaithful F → Faithful F
FullyFaithful→Faithful {F} (ff inv linv rinv) {_} {_} {x} {y} p =
  x             ≡⟨ sym (linv _) ⟩
  inv (F₁ F x)  ≡⟨ ap inv p ⟩
  inv (F₁ F y)  ≡⟨ linv _ ⟩
  y             ∎

FullyFaithful→Full : {F : Functor C D} → FullyFaithful F → Full F
FullyFaithful→Full {F} (ff inv linv rinv) H = squash (inv H , rinv H)

Full+Faithful→FullyFaithful : {F : Functor C D}
                            → Full F
                            → Faithful F
                            → FullyFaithful F
Full+Faithful→FullyFaithful {F} full faithful = r where
  open FullyFaithful

  unique-fibers : {x y : _} (f : D.Hom (F₀ F x) (F₀ F y))
                → (f1 f2 : Σ (C.Hom x y) λ g → F₁ F g ≡ f)
                → f1 ≡ f2
  unique-fibers f (g , Fg≡f) (g' , Fg'≡f) =
    Σ-Path! (faithful (Fg≡f ∘p sym Fg'≡f)) (UIP _ _)

  choose : {x y : _} (f : D.Hom (F₀ F x) (F₀ F y))
         → Σ (C.Hom x y) λ g → F₁ F g ≡ f
  choose f = unsquash' (λ _ → record { carrier = _ ; prop = unique-fibers f })
                       (λ x → x) (full f)

  r : FullyFaithful F
  inv r x = Σ.fst (choose x)
  linv r x = ap Σ.fst (unique-fibers (F₁ F x) (choose (F₁ F x)) (x , refl))
  rinv r x = Σ.snd (choose x)
```
