open import 1Lab.Base

open import Category

import Category.Associativity.Solver

module Category.Associativity {o₁ h₁ : _} (C : Category o₁ h₁) where

private
  module C = Category.Category C

open Category.Associativity.Solver C

open C
open C using (assoc₁ ; assoc₂) public

private
  variable
    a b c d e : C.Ob
    f g h i j k : C.Hom a b

right→left : f ∘ g ∘ h ≡ (f ∘ g) ∘ h
right→left = assoc₂ _ _ _

left→right : (f ∘ g) ∘ h ≡ f ∘ g ∘ h
left→right = assoc₁ _ _ _

second2-out-of-4 : f ∘ g ∘ h ∘ i ≡ f ∘ (g ∘ h) ∘ i 
second2-out-of-4 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            (_ ↑ ∘' (_ ↑ ∘' _ ↑) ∘' _ ↑)
            refl

first3-out-of-4 : f ∘ g ∘ h ∘ i ≡ (f ∘ g ∘ h) ∘ i 
first3-out-of-4 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            ((_ ↑ ∘' _ ↑ ∘' _ ↑) ∘' _ ↑)
            refl

first2-out-of-4 : f ∘ g ∘ h ∘ i ≡ (f ∘ g) ∘ h ∘ i 
first2-out-of-4 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            ((_ ↑ ∘' _ ↑) ∘' _ ↑ ∘' _ ↑)
            refl

third2-out-of-4 : f ∘ g ∘ h ∘ i ≡ f ∘ g ∘ (h ∘ i) 
third2-out-of-4 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            (_ ↑ ∘' _ ↑ ∘' (_ ↑ ∘' _ ↑))
            refl

second3-out-of-5 : f ∘ g ∘ h ∘ i ∘ j ≡ f ∘ (g ∘ h ∘ i) ∘ j
second3-out-of-5 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            (_ ↑ ∘' (_ ↑ ∘' _ ↑ ∘' _ ↑) ∘' _ ↑)
            refl

first3-out-of-5 : f ∘ g ∘ h ∘ i ∘ j ≡ (f ∘ g ∘ h) ∘ i ∘ j
first3-out-of-5 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            ((_ ↑ ∘' _ ↑ ∘' _ ↑) ∘' _ ↑ ∘' _ ↑)
            refl

first4-out-of-5 : f ∘ g ∘ h ∘ i ∘ j ≡ (f ∘ g ∘ h ∘ i) ∘ j
first4-out-of-5 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            ((_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑) ∘' _ ↑)
            refl

first2-out-of-5 : f ∘ g ∘ h ∘ i ∘ j ≡ (f ∘ g) ∘ h ∘ i ∘ j
first2-out-of-5 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            ((_ ↑ ∘' _ ↑) ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            refl

second2-out-of-5 : f ∘ g ∘ h ∘ i ∘ j ≡ f ∘ (g ∘ h) ∘ i ∘ j
second2-out-of-5 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            (_ ↑ ∘' (_ ↑ ∘' _ ↑) ∘' _ ↑ ∘' _ ↑)
            refl

third2-out-of-5 : f ∘ g ∘ h ∘ i ∘ j ≡ f ∘ g ∘ (h ∘ i) ∘ j
third2-out-of-5 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            (_ ↑ ∘' _ ↑ ∘' (_ ↑ ∘' _ ↑) ∘' _ ↑)
            refl

first3-out-of-6 : f ∘ g ∘ h ∘ i ∘ j ∘ k ≡ (f ∘ g ∘ h) ∘ i ∘ j ∘ k 
first3-out-of-6 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            ((_ ↑ ∘' _ ↑ ∘' _ ↑) ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            refl

third2-out-of-6 : f ∘ g ∘ h ∘ i ∘ j ∘ k ≡ f ∘ g ∘ (h ∘ i) ∘ j ∘ k 
third2-out-of-6 =
  associate (_ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑ ∘' _ ↑)
            (_ ↑ ∘' _ ↑ ∘' (_ ↑ ∘' _ ↑) ∘' _ ↑ ∘' _ ↑)
            refl
