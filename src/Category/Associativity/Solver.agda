open import 1Lab.Base

open import Category

module
  Category.Associativity.Solver
  {o h : _} (C : Category o h)
  where

  private
    module C = Category.Category C

  open C

  data Expr : C.Ob → C.Ob → Set (o ⊔ h) where
    id'  : {A : _}     → Expr A A
    _∘'_ : {A B C : _} → Expr B C → Expr A B → Expr A C
    _↑   : {A B : _}   → C.Hom A B → Expr A B
  
  infixr 40 _∘'_
  infix 50 _↑

  embed : {A B : _} → Expr A B → C.Hom A B
  embed id'       = id
  embed (x ↑)     = x
  embed (x ∘' x₁) = embed x ∘ embed x₁

  eval : {A B C : _} → Expr B C → C.Hom A B → C.Hom A C
  eval id' f       = f
  eval (x ↑) f     = x ∘ f
  eval (e ∘' e₁) f = eval e (eval e₁ f)

  eval≡ : {A B : _} (e : Expr A B) → eval e id ≡ embed e
  eval≡ id' = refl
  eval≡ (x ↑) = C.idr _
  eval≡ (f ∘' g) =
    eval f (eval g id)    ≡⟨ sym (lemma f (eval g id)) ⟩
    eval f id ∘ eval g id ≡⟨ ap₂ _∘_ (eval≡ f) (eval≡ g) ⟩
    embed (f ∘' g)        ∎
    where
    lemma : {A B C : _} (e : Expr B C) (f : C.Hom A B) → eval e id ∘ f ≡ eval e f
    lemma id' f = C.idl _
    lemma (x ↑) f = ap (λ e → e C.∘ f) (C.idr x) 
    lemma (f ∘' g) h =
      eval f (eval g id) ∘ h      ≡⟨ ap (λ e → e ∘ h) (sym (lemma f (eval g id))) ⟩
      (eval f id ∘ eval g id) ∘ h ≡⟨ C.assoc₁ _ _ _ ⟩
      eval f id ∘ eval g id ∘ h   ≡⟨ ap (λ e → _ ∘ e) (lemma g h) ⟩
      eval f id ∘ eval g h        ≡⟨ lemma f (eval g h) ⟩
      eval (f ∘' g) h             ∎

  associate : {A B : _} (f g : Expr A B) → eval f id ≡ eval g id → embed f ≡ embed g
  associate f g p = sym (eval≡ f) ∘p p ∘p eval≡ g
