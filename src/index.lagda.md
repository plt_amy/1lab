# 1lab

A formalised, discoverable reference resource for 1-category theory,
done in Agda. **Formalised** meaning that all of the mathematical
development is checked by a proof assistant (namely, Agda);
**Discoverable** meaning that the 1Lab is a _web_: Every bit of text
that is in colour and not in boldface is a link. Don't know what a
`Category`{.Agda} is? Click right there!

Links are colour-coded to indicate what they point to. In body text,
links rendered in [blue (or purple) sans-serif font](index.html) link to
_pages_; Links rendered in one of the syntax highlighting colours and
`monospace`{.agda ident=Category} link to a _definition_. Specifically,
the following colours are used:

* Blue for records, functions and postulates: `Category`{.agda ident=Category.Category}, `sym`{.agda}, `fun-ext`{.agda}
* Green for inductive and coinductive constructors: `refl`{.agda}
* Bold maroon for modules: `Category`{.agda ident=Category}
* Purple for record selectors: `Category.id`{.agda}

Contrary to the [`agda-categories` library], the 1Lab does not aim to
be reusable. Because of this, the code is unconventional. In addition to
not using the Agda standard library (the author has a case of NIH
syndrome), and the following postulates are used. They are described in
more details in the page [1Lab.foundations].

[`agda-categories` library]: https://github.com/agda/agda-categories
[1Lab.foundations]: agda://1Lab.foundations

* `function extensionality`{.agda ident=fun-ext}
* `propositional extensionality`{.agda ident=prop-ext}
* `propositional resizing`{.agda ident=hProp}
* `dependent elimination for truncations`{.agda ident=unsquash'}
* `existence of coequalisers`{.agda ident=coglue}

The first three postulates amount to saying that the `category of
Sets`{.Agda ident=Sets} is `an elementary topos`{.Agda
ident=ElementaryTopos}, and the latter two are merely a convenience:
every elementary topos is (finitely) cocomplete, so the first three
postulates already imply the existence of coequalizers. However, that
construction is inconvenient to work with: The postulated version
addresses this.

### Technology

The 1Lab uses [Iosevka](https://typeof.net/Iosevka/) as its monospace
typeface. Iosevka is licensed under the SIL Open Font License, v1.1, a
copy of which can be found [here](/static/licenses/LICENSE.Iosevka).

Mathematics is rendered using [KaTeX](https://katex.org), and as so, the
1Lab redistributes KaTeX's fonts and stylesheets, even though the
rendering is done entirely at build-time. KaTeX is licensed under the
MIT License, a copy of which can be found
[here](/static/licenses/LICENSE.KaTeX).

Our favicon is Noto Emoji's cat face, codepoint U+1F431. This is the
only image from Noto we redistribute. Noto fonts are licensed under the
Apache 2.0 License, a copy of which can be found
[here](/static/licenses/LICENSE.Noto).

Commutative diagrams appearing in body text are created using
[quiver](https://q.uiver.app), and rendered to SVG using a combination of
[rubber-pipe](https://github.com/petrhosek/rubber) and
[pdftocairo](https://poppler.freedesktop.org/), part of the Poppler
project. No part of these projects is redistributed.

And, of course, the formalisation would not be possible without [Agda](https://github.com/agda/agda).

<!--
```agda
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Base

module index where

_ = _≡_
_ = fun-ext
_ = sym
_ = prop-ext
_ = hProp
```
-->

---

# Category Theory

The theory of [1-categories](agda://Category#Category) is developed in
modules under the `Category` namespace.

```agda
open import Category
```

### Category Instances

In addition to the ones listed here, the module
[`Category`](Category.html) exports [the category of Sets](agda://Category#Sets).

The functors between $C$ and $D$ form a category - A
`FunctorCat`{.Agda}, written $[C, D]$.

```agda
open import Category.Instances.FunctorCat

_ : {o h o₁ h₁ : _} (C : Category o h) (D : Category o₁ h₁)
  → Category (o ⊔ h ⊔ o₁ ⊔ h₁) (o ⊔ h ⊔ h₁)
_ = FunctorCat
```

An important class of functor category are the _presheaf_ categories,
$\operatorname{PSh}(C) = [C^{\operatorname{op}}, \operatorname{Sets}]$.
`PSh`{.Agda} is parametrised by a level `ℓ` which adjusts the universe
of $\operatorname{Sets}$ of the functors.

```agda
open import Category.Instances.PSh

_ : {o h : _} → Category o h → {ℓ : _}
  → Category (lsuc (lsuc (o ⊔ ℓ)) ⊔ h)
             (lsuc (o ⊔ ℓ) ⊔ h)
_ = PSh
```

Categories themselves form a category - `Cat`{.Agda}, though
`Cat`{.Agda} is much larger than the categories that make it up.
This category has a lot of rich structure:

```agda
open import Category.Instances.Cat.Whisker
open import Category.Instances.Cat

_ : (o h : _) → Category (lsuc (o ⊔ h)) (o ⊔ h)
_ = Cat
```

A groupoid is a category where all morphisms are invertible - The
collection of all groupoids in a given universe forms a category,
`Grpd`{.Agda}.

```agda
open import Category.Instances.Grpd

_ : (o h : _) → Category (lsuc (o ⊔ h)) (o ⊔ h)
_ = Grpd
```

An h-proposition is a type that is contractible if inhabited, i.e., a subsingleton.
These assemble into a full subcategory of `Sets`{.Agda}, which is
moreover _reflective_: The ff functor $\mathrm{Props} \hookrightarrow
\mathrm{Sets}$ has a left adjoint (the propositional truncation).

```agda
open import Category.Instances.Props

_ : (l : _) → Category l l
_ = Props
```

### Functors

Characterisation of bifunctors: Functors out of $C
\times_{\operatorname{Cat}} D$, which are automatically functorial in
both their arguments.

```agda
open import Category.Functor.Bifunctor
```

The $\operatorname{Hom}(-,-)$, $\operatorname{Hom}(F-,-)$ and
$\operatorname{Hom}(-,G-)$ functors, and a proof that $F \dashv G$
implies $\operatorname{Hom}(F-,-)$ and $\operatorname{Hom}(-,G-)$ are
naturally isomorphic.

```agda
open import Category.Functor.Hom
```

In a category with pullbacks, an arrow $f : Y \to X$ induces a
_change-of-base_ functor $f^* : C/X \to C/Y$ between the slice
categories.

```agda
open import Category.Functor.Pullback
```

### Limits

Limits are one of the canonical ways of describing when an object is
"made up of" other objects. See more:

```agda
open import Category.Limit.Base
```

<!-- reference anchor for Cones
```agda
_ = Cones
_ = Cone
```
-->

Relationship between the explicitly-constructed pullbacks and pullbacks
considered as limits over a cospan diagram.

```agda
open import Category.Limit.Pullback
```

#### Continuity

A functor is called _continuous_ if it preserves limits. The functor
$\operatorname{Hom}(X, -)$ is continuous, as is every right adjoint.

```
open import Category.Limit.Continuous.Hom
open import Category.Limit.Continuous.Adjoint
```

### Structure

Explicit characterisation of structure a category might possess.

A category is _Cartesian_ if it has a Cartesian product object $A \times
B$ for every pair of objects $A$ and $B$, in addition to a terminal
object: An object $\top$ for which the map $!_A : \top \to A$ is unique.

```agda
open import Category.Structure.Cartesian
open import Category.Structure.Terminal
```

A category is _Cartesian closed_ if it is Cartesian and the product
functor $- \times A$ has a right adjoint.

```agda
open import Category.Structure.CartesianClosed
```

A category is _finitely complete_ if it is Cartesian and, in addition,
has binary pullbacks and binary equalisers.

```agda
open import Category.Structure.FinitelyComplete
```

### Adjunctions

Functors $F : C \to D$ and $G : D \to C$ are adjoints, written $F \dashv
G$, if there are natural transformations $\operatorname{unit} :
\operatorname{Id} \to (G \circ F)$ and $\operatorname{counit} : (F \circ
G) \to \operatorname{Id}$, satisfying the zig-zag identities zig and
zag.

The definition in terms of unit and counit natural transformations was
chosen because it is easier to make polymorphic across the levels of $C$
and $D$. A natural isomorphism between $\operatorname{Hom}$-functors
would mandate either that $C$ and $D$ have their homs in the same
universe, or require lifting both hom-universes to their least upper bound.

```agda
open import Category.Functor.Adjoints
_ = _⊣_.zig
_ = _⊣_.zag
```

#### Monads

```agda
open import Category.Constructions.Monad
open import Category.Functor.Adjoints.Monad
open import Category.Functor.Adjoints.Monadic
```

# Algebra

⚠️ WIP ⚠️

## Algebraic Theories

```agda
open import Algebra.Theory
```

## Monoids

```agda
open import Algebra.Monoid
open import Algebra.Monoid.Endomorphism
open import Algebra.Monoid.EckmannHilton
open import Algebra.Monoid.Category.Monadic
open import Algebra.Monoid.Category.Cartesian
```

<!--
```agda
open import 1Lab.Coeq
open import Topos.Base

_ = Category.Category

-- needs a type signature otherwise Agda can't figure out which refl I mean
_ : {A : Set lzero} {x : A} → x ≡ x
_ = refl

_ = Category.id
_ = unsquash'
_ = coglue
_ = Sets
_ = ElementaryTopos
```
-->
