```
{-# OPTIONS --rewriting #-}
open import 1Lab.Coeq.FunExt
open import 1Lab.Coeq
open import 1Lab.Base

module 1Lab.HProp where

{-# NO_UNIVERSE_CHECK #-}
record hProp ℓ : Set ℓ where
```

# h-Propositions

An [h-proposition] is a type that is (-1)-truncated: One for which, for
all $x, y : A$, we have $x = y$. The `collection of all h-props`{.Agda
ident=hProp} of size $\ell$ is normally of size $1+\ell$, but using `{-#
NO_UNIVERSE_CHECK #-}` gives a weak form of [propositional resizing].

[h-proposition]: 1Lab.foundations.html#propositions

```
  field
    carrier : Set ℓ
    prop : (x y : carrier) → x ≡ y

open hProp
```

```
_∈_ : {ℓ ℓ' : _} {A : Set ℓ} → A → (A → hProp ℓ') → Set ℓ'
x ∈ A = carrier (A x)

⟨_⟩ : {ℓ : _} → hProp ℓ → Set ℓ
⟨_⟩ = hProp.carrier
```

Maps into `hProp`{.Agda} are used to represent subsets, so it makes
sense to use `∈`{.Agda ident="_∈_"} as shorthand syntax for "`f x` is
inhabited".

## Propositional Truncation

```agda
∥_∥ : {ℓ : Level} → Set ℓ → hProp ℓ
```

Since we have [propositional resizing], the propositional truncation
`∥_∥`{.Agda} can be implemented as a double-negation transformation
polymorphic in the return type, à la HoTT book.  This ensures that
unsquash has a definitional computation rule.

[propositional resizing]: 1Lab.foundations.html#propositional-resizing

Being (the object part of) [a left
adjoint](Category.Instances.Props.html#free-propositions), the
propositional truncation can be considered to make "free propositions"
on a set.

```
carrier (∥_∥ {ℓ} P) = (Q : hProp ℓ) → (P → carrier Q) → carrier Q
hProp.prop (∥_∥ {ℓ} P) x y =
  (fun-ext λ { record { carrier = carrier ; prop = prop } →
    fun-ext λ x → prop _ _ })
```

```
∃ : {ℓ ℓ' : _} (A : Set ℓ) (B : A → Set ℓ') → hProp _
∃ A B = ∥ Σ A B ∥
```

Truncated types provide the appropriate interpretation of the
existential quantifier in constructive logic. In particular, an
existential quantifier should be a _proposition_; A $\sum$ type is
classically read as a proof of existence together with a choice of
witness for the existence. This difference can be seen in, for instance,
the difference between a [surjection] and a [split surjection].

[surjection]: agda://1Lab.Function.Properties#surjection
[split surjection]: agda://1Lab.Function.Properties#split-surjection

```
⟨∥_∥⟩ : {ℓ : Level} → Set ℓ → Set ℓ
⟨∥ A ∥⟩ = hProp.carrier ∥ A ∥

squash : {ℓ : Level} {A : Set ℓ} → A → carrier ∥ A ∥
squash x Q r = r x

unsquash : {ℓ : _} {A : Set ℓ} {B : hProp ℓ}
         → (A → hProp.carrier B)
         → carrier ∥ A ∥ → hProp.carrier B
unsquash {A = A} {B = B} f sq = sq B f
```

```
postulate
  unsquash' : {ℓ : _} {ℓ' : _} {A : Set ℓ} (B : hProp.carrier ∥ A ∥ → hProp ℓ')
            → ((x : A) → hProp.carrier (B (squash x)))
            → (x : carrier ∥ A ∥) → hProp.carrier (B x)
```

Unfortunately, the construction does not support elimination, only
recursion. Induction over `∥ X ∥` is one of our four postulates. It
also comes with a computation rule.

```
  unsquash'_squash : {ℓ : _} {A : Set ℓ} {B : hProp.carrier ∥ A ∥ → hProp ℓ}
                   → (f : (x : A) → hProp.carrier (B (squash x)))
                   → (x : A)
                   → unsquash' B f (squash x) ↝ f x
  {-# REWRITE unsquash'_squash #-}
```

```
_ : {ℓ : _} {A : Set ℓ} {B : hProp ℓ} {f : A → hProp.carrier B} {x : A}
  → unsquash {B = B} f (squash x) ↝ f x
_ = refl
```

# Concrete Propositions

```
P⊤ : {ℓ : _} → hProp ℓ
P⊤ = record { carrier = Top ; prop = λ x y → refl }

P⊥ : {ℓ : _} → hProp ℓ
P⊥ = record { carrier = Bot ; prop = λ x () }

_P∧_ : {ℓ ℓ' : _} → hProp ℓ → hProp ℓ' → hProp (ℓ ⊔ ℓ')
carrier (a P∧ b) = carrier a × carrier b
hProp.prop (a P∧ b) (ax , bx) (ay , by) = ×Path (prop a ax ay) (prop b bx by)

_P∨_ : {ℓ ℓ' : _} → hProp ℓ → hProp ℓ' → hProp (ℓ ⊔ ℓ')
a P∨ b = ∥ carrier a ∐ carrier b ∥
```

The universe of propositions has initial and [terminal] objects, is
closed under taking [limits], and gets colimits by [being a reflective
subcategory of Set].

[terminal]: Category.Structure.Terminal.html
[limits]: Category.Limit.Base.html
[being a reflective subcategory of Set]: Category.Instances.Props.html

# Propositional Extensionality

```
-- The propositional extensionality axiom.
postulate
  prop-ext : {ℓ : _} {A B : hProp ℓ}
           → (carrier A → carrier B)
           → (carrier B → carrier A)
           → A ≡! B
```

The other `postulate`{.Agda} relating to h-props is `prop-ext`{.Agda},
saying that propositional equality of h-props is biimplication.

```
  -- And its computation rule:
  prop-ext-transp : {ℓ : _} {A B : hProp ℓ}
                  → {f : carrier A → carrier B}
                  → {g : carrier B → carrier A}
                  → {x : _}
                  → transport (ap hProp.carrier (prop-ext {A = A} {B = B} f g)) x
                  ↝ f x
  {-# REWRITE prop-ext-transp #-}
```
