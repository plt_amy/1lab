```
module 1Lab.foundations where
```

# Foundations

The 1Lab is formalised in [Agda], and thus, familiarity with Agda is a
prerequisite to get the most out of it. Additionally, the language used
is slightly extended through the judicious use of [postulates]. In
particular, the following:

* `propositional extensionality`{.agda ident=prop-ext}
* `propositional resizing`{.agda ident=hProp}
* `dependent elimination for truncations`{.agda ident=unsquash'}
* `existence of coequalisers`{.agda ident=coglue}

Like mentioned in [the index](index.html), the first three of these
postulates boil down to the sentence "`Sets`{.Agda} is an `elementary
topos`{.Agda ident=ElementaryTopos}", and the latter two are merely
convenience.

[Agda]: https://agda.readthedocs.io/en/v2.6.2/
[postulates]: https://agda.readthedocs.io/en/v2.6.2/language/postulates.html

<!--
```
open import 1Lab.Base
open import 1Lab.HProp
open import 1Lab.Coeq
open import 1Lab.Coeq.FunExt
open import 1Lab.Data.Finite
open import Category renaming (Category to Cat)
open import Topos.Base
_ = fun-ext
_ = prop-ext
_ = hProp
_ = hProp.carrier
_ = unsquash'
_ = coglue
_ = Sets
_ = ElementaryTopos
_ = Top
_ = transport
_ = ∥_∥
```
-->

## Function extensionality

Function extensionality is a common extension of Martin-Löf type theory.
The rules of type theory are not strong enough to prove the following
implication, saying that if two functions $f$ and $g$ are such that
$\forall x, f(x) = g(x)$, then $f$ = $g$.

```
_ : {A B : Set} {f g : A → B} → ((x : A) → f x ≡ g x) → f ≡ g
```

While this principle is commonly postulated, in the 1Lab, it follows
from the postulated existence of coequalizers. In particular,
`fun-ext`{.Agda} is a _theorem_ in the presence of coequalizers, not a
postulate. You can click on it (either in the text or in the code below)
to see the proof.

```
_ = fun-ext
```

## Propositions

_Truncatedness_ is a measure of how much interesting information a type
has: The interesting information of a 0-truncated type are its
inhabitants; The interesting information of a 1-truncated type are its
inhabitants, and its _paths_ (proofs of equality), and so forth.

A proposition is a type that is _(-1)-truncated_: The only interesting
information about it is whether it is inhabited at all. This extends
further, to a definition of (-2)-truncatedness. A type is (-2)-truncated
when it is isomorphic to the unit type `Top`{.Agda}.

Concretely, a _proposition_ (or h-proposition) is a type $A$ where, for
all $x, y : A$, we have that $x = y$.

```
is-prop : {A : Set} → ((x y : A) → x ≡ y) → hProp lzero
is-prop p = record { carrier = _ ; prop = p }
```

Agda _does_ support [universes of propositions], but they are
inconvenient for a variety of reasons. For example, you can not
eliminate from `Prop` into `Set`, even when the `Set` you are
eliminating into is an h-proposition. Furthermore, Agda has the rule
that `Prop ℓ : Set (lsuc ℓ)`, meaning that the collection of all
$\ell$-sized propositions is in the next universe up.

[universes of propositions]: https://agda.readthedocs.io/en/v2.6.2/language/prop.html

### Propositional extensionality

The principle of propositional extensionality states equality between
two elements of `hProp`{.Agda} - types that are propositions - is
logical equivalence (bi-implication).

```
_ : {A B : hProp lzero} → (⟨ A ⟩ → ⟨ B ⟩) → (⟨ B ⟩ → ⟨ A ⟩) → A ≡ B
_ = prop-ext
```

This implies, for instance, that every inhabited proposition $P$ is equal to
`P⊤`{.Agda}, which we can use with `transport`{.Agda} to "summon" an
inhabitant of the `carrier`{.Agda ident="hProp.carrier"} of $P$.

### Propositional resizing

Like mentioned above, Agda's built-in `Prop ℓ` universe is, in a sense,
"too big". To show that `Sets ℓ`{.Agda ident=Sets} is an `elementary
topos`{.Agda}, we need to find an inhabitant of `Set ℓ` which represents
all the propositions of size `ℓ`.

```
_ : (ℓ : _) → Set ℓ
_ = hProp
```

This is what `hProp`{.Agda} does. Using a `{-# NO_UNIVERSE_CHECK #-}`
pragma, we can implement a limited form of the [propositional resizing]
axiom. This lets us, for instance, build `propositional
truncations`{.Agda ident="∥_∥"} using a "polymorphic double-negation"
transformation. This is because quantifying over all propositions
doesn't raise the universe level:

[propositional resizing]: https://ncatlab.org/nlab/show/propositional+resizing

```
_ : (ℓ : _) (A : Set ℓ) → Set ℓ
_ = λ ℓ A → (X : hProp ℓ) → A
```

### Propositional truncations

Regardless of the existence of propositional truncations - even with a
definitional computation rule for the recursor `unsquash`{.Agda} - we
can not show that we can do _induction_ on propositional truncations.
This postulate corrects that:

```
_ : {A : Set} (B : ⟨∥ A ∥⟩ → hProp lzero)
  → ((x : A) → ⟨ B (squash x) ⟩)
  → (x : ⟨∥ A ∥⟩) → ⟨ B x ⟩
_ = unsquash'
```

## Coequalizers

A coequalizer is a kind of colimit. The coequalizer of $f, g : A \to B$
can be constructed, in the category of Sets, as the _quotient_ $B/\sim$
where $\sim$ is the relation generated by $f(x) \sim g(x)$ for all $x :
A$.

Postulating the existence of coequalizers is equivalent to postulating
the existence of quotients, since the quotient of $A$ by $\sim$ can be
described using dependent sums as the coequalizer of the two projection
maps $\pi_1, \pi_2 : \sum_{x, y : A} (x \sim y) \to A$.

Coequalizers have a constructor, `coeq`{.Agda},

```
_ : {A B : Set} {f g : A → B} → B → Coeq f g
_ = coeq
```

with the property that for any $x : A$, the images $f(x)$ and $g(x)$ are
equal in the coequalizer:

```
_ : {A B : Set} {f g : A → B} → (x : A) → coeq (f x) ≡ coeq (g x)
_ = coglue
```

The elimination rule says that a map $\mathrm{coeq}(f,g) \to Y$ is a
function $B \to P$ which sends $f(x)$ to the same thing as $g(x)$, for
any $x : A$.

```
_ : {A B : Set} {f g : A → B} {P : Set}
  → (func : B → P)
  → ((x : A) → func (f x) ≡ func (g x))
  → Coeq f g → P
_ = Coequalizer.rec
```

# 1Lab Conventions

This section aims to explain the rationale behind some design decisions
made in the formalisation.

## Record types

The 1Lab makes extensive use of record types. All of the following
concepts are represented using record types:

- `Categories`{.Agda ident=Cat}, `functors`{.Agda ident=Functor},
`natural transformations`{.Agda ident="_=>_"};
- [Adjunctions](agda://Category.Functor.Adjoints#_⊣_), [adjoint
equivalences](agda://Category.Functor.Adjoints.Equivalence#is-⊣Equiv);
- [Algebraic theories](agda://Algebra.Theory#Theory) and their
[models](agda://Algebra.Theory#Model);

<!--
```
_ = Cat
_ = Functor
_ = _=>_
_ = _≅_
```
-->

The common feature of all these definitions is that a natural language
description for them begins like

> A model of an algebraic theory is a set **equipped with** [...] **such
> that** [..]

This indicates that a record should be used. How this record should be
built depends on the specific case: The concept of an adjunction between
functors makes much more sense than an "adjoint pair" in the abstract,
so they are coded with a parametrised record: [`F ⊣
G`](agda://Category.Functor.Adjoints#_⊣_).

## Morphism equality

The definition of adjunctions is illustrative in another way. There are
(among others) two particular definitions of adjunction that are
commonly used:

- Two functors $F : C \to D$ and $G : D \to C$ are adjoints if there is
a `natural isomorphism`{.Agda ident="_≅_"} between [Hom
functors](agda://Category.Functor.Hom) $\mathrm{Hom}_D(F(A), B) \cong
\mathrm{Hom}_C(A, G(B))$.

- Two functors $F : C \to D$ and $G : D \to C$ are adjoints if there are
natural transformations $\eta : \mathrm{Id} \Rightarrow (G \circ F)$
(the [unit](agda://Category.Functor.Adjoints#unit)) and $\epsilon : (F
\circ G) \Rightarrow \mathrm{Id}$ (the
[counit](agda://Category.Functor.Adjoints#counit)) satisfying the
triangle identities [zig](agda://Category.Functor.Adjoints#zig) and
[zag](agda://Category.Functor.Adjoints#zag).

The latter definition, while equivalent to the former, is preferred,
since it does not fix the universe levels in which the `Hom` sets live.
This difference can be seen when comparing the types of
[`_⊣_`](agda://Category.Functor.Adjoints#_⊣_) and
[`adjoint→hom≃`](agda://Category.Functor.Adjoints#adjoint→hom≃). In the
natural isomorphism formulation, both categories must have their
`Hom`-sets in the same universe, `h`.

## Equational reasoning

```
open import 1Lab.Base
```

The `1Lab.Base`{.Agda} module defines the `_≡⟨_⟩_`{.Agda} and
`_∎`{.Agda} combinators.  These are used to build _equational reasoning_
chains, for instance in the following proof that addition is
associative:

```
+-associative : (x y z : _) → (x + y) + z ≡! x + (y + z)
+-associative zero y z = refl
+-associative (suc x) y z =
  suc ((x + y) + z) ≡⟨ ap suc (+-associative x y z) ⟩
  suc (x + (y + z)) ∎
```

<!--
```
_ = _≡⟨_⟩_
_ = _∎
```
-->

If your browser runs JavaScript, these equational reasoning chains, by
default, render with the _justifications_ (the argument written between
`⟨ ⟩`) hidden; There is a button to display them, either on the sidebar
or on the top bar depending on how narrow your screen is. For your
convenience, it's here too:

<div style="display: flex; flex-direction: column; align-items: center;">
<button type="button" class="equations">
  Toggle equations
</button>
</div>

Try pressing it! A slightly less trivial use of these is in the
formalisation of the [Eckmann-Hilton
argument](agda://Algebra.Monoid.EckmannHilton#Eckmann-Hilton), under the
Algebra namespace.

## Duality

In the 1Lab, dual concepts are deduplicated. A prominent example is in
the definition of an `elementary topos`{.Agda ident=ElementaryTopos}.
Since elementary topoi are all finitely cocomplete, for convenience, the
definition includes having finite _coproducts_. However, nowhere in the
library there is a definition of what it means for a category to have
\finite coproducts!

Instead, we ask that an elementary topos $\mathcal{E}$ have finite
coproducts by asking that $\mathcal{E}^{\text{op}}$ have finite
products, which here is called a [Cartesian category].

[Cartesian category]: agda://Category.Structure.Cartesian#Cartesian

## "Concrete diagrams"

In contrast with duality, where dual concepts are duplicated, diagrams
of "important shape" are defined in a concrete way, and, if applicable,
related to a more general definition. As an example, there is a record
type that indicates that a specific square is a [Pullback]. This
provides convenient access to.. _everything_ involved: Commutativity of
the square, existence of the arrow that makes the square into a limit,
uniqueness of that arrow, etc.

[Pullback]: agda://Category.Diagrams#Pullback

There is also an abstract definition of [limit], in terms of cones over
a diagram.  This definition is vastly more general, and thus, less
convenient. There is [a theorem] that relates the concrete definition of
pullbacks and limits over a [cospan diagram], and this is used in the
proof that [continuous functors] [preserve monomorphisms].

[limit]: agda://Category.Limit.Base
[a theorem]: agda://Category.Limit.Pullback#Pullback→Diagram
[cospan diagram]: agda://Category.Limit.Pullback#Cospan
[continuous functors]: agda://Category.Limit.Base#Continuous
[preserve monomorphisms]: agda://Category.Limit.Pullback#Continuous→Monics
