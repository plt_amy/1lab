open import 1Lab.Coeq.Base
open import 1Lab.Base

module 1Lab.Coeq.FunExt where

[0,1] : Set
[0,1] = Coeq {A = (Bool × Bool)} _×_.fst _×_.snd

i0 : [0,1]
i0 = coeq tt

i1 : [0,1]
i1 = coeq ff

seg : i0 ≡ i1
seg = coglue (tt , ff)

fun-ext : {ℓ ℓ' : _} {A : Set ℓ} {B : A → Set ℓ'}
        → {f g : (x : A) → B x}
        → ((x : A) → f x ≡ g x)
        → f ≡ g
fun-ext {A = A} {B} {f} {g} hom = ap h' seg where
  h : (x : A) -> [0,1] -> B x
  h x = Coequalizer.rec (λ { tt → f x ; ff -> g x }) λ where
    (tt , tt) → refl
    (tt , ff) → hom x
    (ff , tt) → sym (hom x)
    (ff , ff) → refl

  h' : [0,1] -> (x : A) -> B x
  h' x y = h y x

fun-ext-invis : {ℓ ℓ' : _} {A : Set ℓ} {B : A → Set ℓ'}
              → {f g : {x : A} → B x}
              → ((x : A) → f {x} ≡ g {x})
              → _≡_ {ℓ ⊔ ℓ'} {A = {x : A} → B x} f {B = {x : A} → B x} g
fun-ext-invis {ℓ} {ℓ'} {A} {B} {f} {g} p = ap fixup (fun-ext p) where
  fixup : ((x : A) → B x) → {x : A} → B x
  fixup f {x} = f x