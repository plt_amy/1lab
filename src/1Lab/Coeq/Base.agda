open import 1Lab.Base

module 1Lab.Coeq.Base where

module Coequalizer {a b : _} {A : Set a} {B : Set b} {f g : B → A} where
  private
    record $ (X : Set a) : Set a where
      constructor $-in
      field
        $-out : X

  set : Set a
  set = $ (Top {a} → $ A)

  coeq : A → set
  coeq x = $-in (λ { tt → $-in x })

  postulate
    coglue : (x : B) → coeq (f x) ≡! coeq (g x)

  elim : {p : _} (P : set → Set p)
       → (func : (x : A) → P (coeq x))
       → ((y : B) → subst P (coglue y) (func (f y)) ≡! func (g y))
       → (x : set) → P x
  elim P func resp ($-in out) = func ($.$-out (out tt))

  rec : {p : _} {P : Set p}
      → (func : A → P)
      → ((y : B) → func (f y) ≡! func (g y))
      → set → P
  rec func resp = elim (λ _ → _) func (λ y → substConst ∘p resp y)

Coeq : {a b : _} {A : Set a} {B : Set b}
     → (f g : A → B)
     → Set _
Coeq f g = Coequalizer.set {f = f} {g = g}

open Coequalizer using (coglue ; coeq) public