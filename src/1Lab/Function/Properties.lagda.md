```agda
open import 1Lab.Coeq.FunExt
open import 1Lab.HProp
open import 1Lab.Base

open import Category

open Functor

module
  1Lab.Function.Properties
  {ℓ ℓ' : _} {A : Set ℓ} {B : Set ℓ'}
  where
```

# Properties of functions

```
surjection : (A -> B) → Set _
split-surjection : (A -> B) → Set _
```

A _surjection_ is a function $f$ where, for each $y$ in the codomain,
there exists an $x$ such that $f(x) = y$. The existence of this preimage
does not imply a _choice_ of preimage: That is a
`split-surjection`{.Agda}.

```
surjection f = (y : B) → ⟨ (∃ A λ x → f x ≡ y) ⟩
split-surjection f = (y : B) → Σ A λ x → f x ≡ y
```

---

```
injection : (A → B) → Set _
```

An _injection_ is a function that "reflects equality": If $f(x) = f(y)$,
then $x = y$. Classically this is the same as "preserving inequality":
$x \not= y \to f(x) \not= f(y)$.

```
injection f = {x y : _} → f x ≡ f y → x ≡ y
```

---

```
bijection : (A → B) → Set _
```

A bijection is a function that is both injective and surjective. Since
injectivity implies uniqueness of fibers, bijections are the same as
[isomorphisms](agda://1Lab.Isomorphism).

```
bijection f = injection f × surjection f
```

---

⚠️ WIP ⚠️

```
monic : (A → B) → Setω
monic f = {ℓ : _} {C : Set ℓ} (g₁ g₂ : C → A)
        → (λ x → f (g₁ x)) ≡! (λ x → f (g₂ x)) → g₁ ≡ g₂

epic : (A → B) → Setω
epic f = {ℓ : _} {C : Set ℓ} (g₁ g₂ : B → C)
       → (λ x → g₁ (f x)) ≡! (λ x → g₂ (f x)) → g₁ ≡ g₂

epic→surjection : (f : A → B)  → epic f → surjection f
epic→surjection f epic y = transport (ap hProp.carrier (happly inhabited y)) tt
  where
    g₁ : B → hProp (ℓ ⊔ ℓ')
    g₁ _ = P⊤

    g₂ : B → hProp (ℓ ⊔ ℓ')
    g₂ y = ∃ A λ x → f x ≡ y

    inhabited =
      epic g₁ g₂ (fun-ext (λ x → prop-ext (λ _ → squash (x , refl))
                          λ _ → tt))
```
