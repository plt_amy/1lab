open import 1Lab.Isomorphism.Reasoning
open import 1Lab.Isomorphism
open import 1Lab.Base

open import Category.Structure.CartesianClosed
open import Category.Structure.Cartesian
open import Category.Functor.Adjoints
open import Category

open import Topos.Base

import Category.Diagrams

module Topos.Logic {o h : _} {C : Category o h} (E : ElementaryTopos C) where
  private
    module E = ElementaryTopos E renaming (_×C_ to _×_)

  open E
  open Category.Diagrams C

  ⊥-unique : {c : Ob} → ({a : Ob} → isContr (Hom c a)) → c ≈ ⊥
  ⊥-unique {obj} maps = 
    isContr.center maps
    , iso E.absurd
          (sym (isContr.paths initial _) ∘p isContr.paths initial _)
          (sym (isContr.paths maps _) ∘p isContr.paths maps _)

  X×⊥-initial : {C A : Ob} → isContr (Hom (⊥ E.× C) A)
  X×⊥-initial {C} {A} = isContr-≃ initial (morp ≃¯¹) where
    morp = Hom (⊥ E.× C) A   ≃⟨ adjoint→≃ _ _ (Tensor⊣Hom E.closed _) ⟩
           Hom ⊥ E.[ C , A ] ≃⟨ isContr→≃ initial initial ⟩
           Hom ⊥ A           ≃∎

  diagonal : {c : Ob} → c ↪ (c E.× c)
  diagonal = record { map = id h× id ; monic = e } where
    e : is-Mono (id h× id)
    e {g₁ = g₁} {g₂} x =
      h×≡h×→f≡f E.cartesian (subst (λ e → _×_.fst e ≡ _×_.snd e) (×Path (p g₁) (p g₂)) x)
      where
        p : {x y : _} (a : E.Hom x y) → _
        p a =
          (id h× id) ∘ a       ≡⟨ h×-∘-dist E.cartesian ⟩
          (id ∘ a) h× (id ∘ a) ≡⟨ ap₂ _h×_ (idl _) (idl _) ⟩
          a h× a               ∎

  internal≡ : {c : Ob} → Hom (c E.× c) Ω
  internal≡ = E.χ diagonal

  ⌜_⌝ : {x y : Ob} → Hom x y → Hom ⊤ [ x , y ]
  ⌜ f ⌝ = λf (f ∘ π₂)

  name = ⌜_⌝

  points-are-monic : {a : Ob} {f : Hom ⊤ a}
                   → is-Mono f
  points-are-monic _ = sym (isContr.paths terminal _) ∘p isContr.paths terminal _

  ∧ : Hom (Ω E.× Ω) Ω
  ∧ = χ record { map = true h× true ; monic = points-are-monic }
  
  ∀ₐ : {a : _} → Hom [ a , Ω ] Ω
  ∀ₐ = χ record { map = ⌜ true ∘ ! ⌝ ; monic = points-are-monic }
  
  false : Hom ⊤ Ω
  false = ∀ₐ ∘ ⌜ id ⌝

  ¬ : Hom Ω Ω
  ¬ = χ record { map = false ; monic = points-are-monic }

  ⇒ : Hom (Ω E.× Ω) Ω
  ⇒ = internal≡ ∘ (∧ h× π₁)

  ∨ : Hom (Ω E.× Ω) Ω
  ∨ = ∀ₐ ∘ λf (⇒ ∘ (∧ ∘ (⇒ ∘ (π₁ ∘ π₁ h× π₂) h× ⇒ ∘ (π₂ ∘ π₁ h× π₂)) h× π₂))

  ∃ₐ : {a : _} → Hom [ a , Ω ] Ω
  ∃ₐ = ∀ₐ ∘ λf (⇒ ∘ ((∀ₐ ∘ π₁) h× π₂))

  Classified-by : {A : _} → Hom A Ω → Ob
  Classified-by p = Equaliser-of p (true ∘ !)

  Classified-by↪A : {A : _} (f : Hom A Ω)
                  → Classified-by f ↪ A
  Classified-by↪A f = record { map = equalise f (true ∘ !) ; monic = Equaliser.monic is-equaliser }

  module Internal where
    data Type : Set o
    data Context : Set o

    data Type where
      ob   : E.Ob → Type
      ω    : Type
      _~>_ : Type → Type → Type
    
    τ⟦_⟧ : Type → Ob
    τ⟦ ob x ⟧ = x
    τ⟦ ω ⟧ = Ω
    τ⟦ x ~> x₁ ⟧ = [ τ⟦ x ⟧ , τ⟦ x₁ ⟧ ]

    data Context where
      _,_ : Context → Type → Context
      nil : Context
    
    Γ⟦_⟧ : Context → Ob
    Γ⟦ x₁ , x ⟧ = Γ⟦ x₁ ⟧ E.× τ⟦ x ⟧
    Γ⟦ nil ⟧ = ⊤

    data var : Context → Type → Set o where
      here  : {o : _} {Γ : _}   → var (Γ , o) o
      there : {o x : _} {Γ : _} → var Γ o → var (Γ , x) o

    var⟦_⟧ : {Γ : _} {τ : _} → var Γ τ → Hom Γ⟦ Γ ⟧ τ⟦ τ ⟧
    var⟦ here ⟧ = π₂
    var⟦ there x ⟧ = var⟦ x ⟧ ∘ π₁

    data _⊢_ : Context → Type → Set o where
      ref : {Γ : _} {τ : _} → var Γ τ       → Γ ⊢ τ
      lam : {Γ : _} {τ σ : _} → (Γ , τ) ⊢ σ → Γ ⊢ (τ ~> σ)
      app : {Γ : _} {τ σ : _} → Γ ⊢ (τ ~> σ)
                              → Γ ⊢ τ
                              → Γ ⊢ σ
      tru  : {Γ : _} → Γ ⊢ ω

      and : {Γ : _} → Γ ⊢ ω → Γ ⊢ ω → Γ ⊢ ω
      all : {Γ : _} {α : _} → Γ ⊢ (α ~> ω) → Γ ⊢ ω

      _⊃_ : {Γ : _} → Γ ⊢ ω → Γ ⊢ ω → Γ ⊢ ω
      _≡?_ : {Γ : _} {α : _} → Γ ⊢ α → Γ ⊢ α → Γ ⊢ ω

    ⟦_⟧ : {Γ : _} {τ : _} → Γ ⊢ τ → Hom Γ⟦ Γ ⟧ τ⟦ τ ⟧
    ⟦ ref x ⟧ = var⟦ x ⟧
    ⟦ lam x ⟧ = λf ⟦ x ⟧
    ⟦ app f arg ⟧ = eval ∘ (⟦ f ⟧ h× ⟦ arg ⟧)
    ⟦ tru ⟧ = true ∘ E.!
    ⟦ all x ⟧ = ∀ₐ ∘ ⟦ x ⟧
    ⟦ and x y ⟧ = ∧ ∘ (⟦ x ⟧ h× ⟦ y ⟧)
    ⟦ x ⊃ y ⟧ = ⇒ ∘ (⟦ x ⟧ h× ⟦ y ⟧)
    ⟦ x ≡? y ⟧ = internal≡ ∘ (⟦ x ⟧ h× ⟦ y ⟧)

    nil⟦_⟧ : {τ : _} → nil ⊢ τ → Hom ⊤ τ⟦ τ ⟧
    nil⟦_⟧ = ⟦_⟧

    ext : ∀ {Γ Δ}
        → (∀ {α : _} → var Γ α → var Δ α)
        → (∀ {α β : _} → var (Γ , β) α → var (Δ , β) α)
    ext ren here = here
    ext ren (there x) = there (ren x)

    rename : ∀ {Γ Δ} {α}
           → (∀ {α : _} → var Γ α → var Δ α)
           → Γ ⊢ α → Δ ⊢ α
    rename ren (ref x) = ref (ren x)
    rename ren (lam x) = lam (rename (ext ren) x)
    rename ren (app x x₁) = app (rename ren x) (rename ren x₁)
    rename ren tru = tru
    rename ren (and x x₁) = and (rename ren x) (rename ren x₁)
    rename ren (all x) = all (rename ren x)
    rename ren (x ⊃ x₁) = rename ren x ⊃ rename ren x₁
    rename ren (x ≡? x₁) = rename ren x ≡? rename ren x₁

    exists : {Γ : _} {α : _} → Γ ⊢ (α ~> ω) → Γ ⊢ ω
    exists {α = α} prop = all {α = ω} (lam (all {α = α} (lam (app (rename there (rename there prop)) (ref here))) ⊃ ref here))

    exists! : {Γ : _} {α : _} → Γ ⊢ (α ~> ω) → Γ ⊢ ω
    exists! {α = α} prop = exists {α = α} (lam (and (app (rename there prop) (ref here)) (all {α = α} (lam (app (rename there (rename there prop))
                                                                                                                (ref here)
                                                                                                          ⊃ (ref here ≡? ref (there here)))))))

    or : {Γ : _} → Γ ⊢ ω → Γ ⊢ ω → Γ ⊢ ω
    or a b = all {α = ω} (lam ((and (rename there a ⊃ ref here) (rename there b ⊃ ref here)) ⊃ ref here))

    not : {Γ : _} → Γ ⊢ ω → Γ ⊢ ω
    not p = p ⊃ (all {α = ω} (lam (ref here)))
