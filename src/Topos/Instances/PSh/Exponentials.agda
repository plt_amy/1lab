open import 1Lab.Coeq
open import 1Lab.Base

open import Category.Structure.CartesianClosed
open import Category.Structure.Cartesian
open import Category.Equality
open import Category

import Category.Associativity
import Category.Instances.PSh

import Topos.Instances.PSh.Limits

open Functor
open _=>_

module Topos.Instances.PSh.Exponentials {o₁ : _} (C : Category o₁ o₁) where
  private
    module C = Category.Category C
    module Cop = Category.Category (C ^op)
    module AssC = Category.Associativity C

  open Category.Instances.PSh C
  open Topos.Instances.PSh.Limits C
  
  open CartesianClosed

  PSh-Closed : CartesianClosed (PSh-Cartesian {o₁})
  PSh-Closed = r where
    module Ca = Cartesian (PSh-Cartesian {o₁})
    module PSh = Category.Category (PSh {o₁})
    module Sets = Category.Category (Sets (lsuc o₁))

    hom : PSh.Ob → PSh.Ob → PSh.Ob
    hom A B = f where
      module A = Functor A
      module B = Functor B

      nt : {x y : _} {hom : _} {act : _}
         → (x₁ : C.Ob)
         → Lift (C.Hom x₁ y) × A.₀ x₁
         → B.₀ x₁
      nt {hom = hom} {act} x (lift f , snd) = η hom x (lift (act C.∘ f) , snd)

      f1 : (x y : _) (act : C.Hom y x) (hom : _) → (よ₀ y Ca.×C A) => B
      f1 x y act hom = NT (nt {x} {y} {hom} {act}) isn where
        isn : _
        isn x₁ y₁ f₁ = fun-ext λ where
          (lift f , snd) →
            η hom y₁ (lift (act C.∘ f C.∘ f₁) , A.F₁ f₁ snd)    ≡⟨ ap (λ e → η hom y₁ (lift e , A.₁ f₁ snd)) AssC.right→left ⟩
            η hom y₁ (lift ((act C.∘ f) C.∘ f₁) , A.F₁ f₁ snd)  ≡⟨ ap (λ e → e (lift (act C.∘ f), snd)) (is-natural hom _ _ _) ⟩
            B.F₁ f₁ (η hom x₁ (lift (act C.∘ f) , snd))         ∎

      f : Category.Ob (PSh {o₁})
      F₀ f x = (よ₀ x Ca.×C A) => B
      F₁ f {x} {y} act hom = f1 x y act hom

      F-id f = fun-ext λ x → NT≡ λ y → fun-ext λ where
        (lift unlift , snd) →
          η x y (lift (C.id C.∘ unlift) , snd) ≡⟨ ap (λ e → η x y (lift e , _)) (C.idl _) ⟩
          η x y (lift unlift , snd)            ∎
      F-∘ f {a} {b} {c} g h = fun-ext λ x →
        NT≡ {n1 = f1 a c (h C.∘ g) x}
            {n2 = f1 b c g (f1 a b h x)}
            λ y → fun-ext λ where
          (lift unlift , snd) →
              η x y (_ , _) ≡⟨ ap (λ e → η x y (lift e , snd)) (C.assoc₁ h g unlift) ⟩
              η x y (_ , _) ∎

    evl : {A B : PSh.Ob} → PSh.Hom (hom A B Ca.×C A) B
    evl {A} {B} = NT eta isn where
      eta : (x : C.Ob) → ((よ₀ x Ca.×C A) => B) × F₀ A x → F₀ B x
      eta x (fst , snd) = η fst x (lift C.id , snd)

      isn : (x y : C.Ob) (f : C.Hom y x) → _
      isn x y f = fun-ext λ where
        (fst , snd) →
          η fst y (lift (f C.∘ C.id) , F₁ A f snd) ≡⟨ ap (λ e → η fst y (lift e , F₁ A _ _)) (C.idr _ ∘p sym (C.idl _)) ⟩
          η fst y (lift (C.id C.∘ f) , F₁ A f snd) ≡⟨ ap (λ e → e (lift C.id , snd)) (is-natural fst x y f) ⟩
          _                                        ∎

    r : CartesianClosed _
    [_,_] r = hom
    eval r {A} = evl {A}

    λf r {a} {b} {c} nat = NT eta isn where
      eta : (x : C.Ob) → F₀ a x → (よ₀ x Ca.×C b) => c
      eta x arg = NT (λ { x' (lift h , o) → η nat _ (F₁ a h arg , o) })
        λ x₁ y f → fun-ext λ where
        (lift unlift , snd) →
          η nat y (F₁ a (unlift C.∘ f) arg , F₁ b f snd)  ≡⟨ ap (λ e → η nat y (e arg , F₁ b f snd)) (F-∘ a _ _) ⟩
          η nat y (F₁ a f (F₁ a unlift arg) , F₁ b f snd) ≡⟨ ap (λ e → e (F₁ a unlift arg , snd)) (is-natural nat _ _ _) ⟩
          F₁ c f (η nat x₁ (F₁ a unlift arg , snd))       ∎

      isn : (x y : C.Ob) (f : C.Hom y x) → _
      isn x y f = fun-ext λ res → NT≡ λ coord → fun-ext λ where
        (lift unlift , snd) →
          η nat coord (F₁ a unlift (F₁ a f res) , snd) ≡⟨ ap (λ e → η nat coord (e res , snd)) (sym (F-∘ a _ _)) ⟩
          η nat coord (F₁ a (f C.∘ unlift) res , snd)  ∎


    β r {a} g = NT≡ λ x → fun-ext λ where
      (fst , snd) → ap (λ e → η g x (e fst , snd)) (F-id a)

    λ-unique r {a} {b} {c} {g} h nt≡ =
      NT≡ λ x → fun-ext λ y → NT≡ λ z → fun-ext λ where
        (lift f , snd) → eq x y z f snd
      where
        eq : (x : _) (y : _) (z : _) (f : _) (snd : _)
           → η (η h x y) z (lift f , snd) ≡ η g z (F₁ a f y , snd)
        eq x y z f snd = sym (
          η g z (F₁ a f y , snd)                   ≡⟨ ap (λ e → η e z (_ , _)) (sym nt≡) ⟩
          η (η h z (F₁ a f y)) z (lift C.id , snd) ≡⟨ ap (λ e → η e z (lift C.id , snd)) (happly (is-natural h _ _ _) _) ⟩
          η (η h x y) z (lift (f C.∘ C.id) , snd)  ≡⟨ ap (λ e → η (η h x y) z (lift e , snd)) (C.idr _) ⟩
          η (η h x y) z (lift f , snd)             ∎
          )
